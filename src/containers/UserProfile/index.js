/** @format */

import React, { PureComponent } from "react";
// import { View, ScrollView, Text, Switch, AsyncStorage, ImageBackground } from "react-native";
import { connect } from "react-redux";
import { Text, View, StyleSheet, ScrollView, Dimensions, TouchableOpacity, Image, TextInput, ImageBackground,Picker, Alert } from 'react-native';
const {height, width} = Dimensions.get('window');
import {
  UserProfileHeader,
  UserProfileItem,
  ModalBox,
  CurrencyPicker,
} from "@components";
import { Languages, Color, Tools, Config, withTheme } from "@common";
import { getNotification } from "@app/Omni";
import _ from "lodash";
import DatePicker from 'react-native-datepicker'

import styles from "./styles";
import MyHistory from './MyHistory';
class UserProfile extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      pushNotification: false,
      clickedState:'info',
    };
  }

  setClickedState = clickedState =>{
    this.setState({clickedState});
  }

  componentDidMount() {
    this._getNotificationStatus();
  }

  _getNotificationStatus = async () => {
    const notification = await getNotification();
    this.setState({ pushNotification: notification || false });
  };

  /**
   * TODO: refactor to config.js file
   */
  _getListItem = () => {
    const { currency, wishListTotal, userProfile, isDarkTheme } = this.props;
    const listItem = [...Config.ProfileSettings];
    const items = [];
    let index = 0;

    for (let i = 0; i < listItem.length; i++) {
      const item = listItem[i];
      if (item.label === "PushNotification") {
        item.icon = () => (
          <Switch
            onValueChange={this._handleSwitchNotification}
            value={this.state.pushNotification}
            tintColor={Color.blackDivide}
          />
        );
      }
      if (item.label === "DarkTheme") {
        item.icon = () => (
          <Switch
            onValueChange={this._onToggleDarkTheme}
            value={isDarkTheme}
            tintColor={Color.blackDivide}
          />
        );
      }
      if (item.label === "Currency") {
        item.value = currency.code;
      }

      if (item.label === "WishList") {
        items.push({
          ...item,
          label: `${Languages.WishList} (${wishListTotal})`,
        });
      } else {
        items.push({ ...item, label: Languages[item.label] });
      }
    }

    if (!userProfile.user) {
      index = _.findIndex(items, (item) => item.label === Languages.Address);
      if (index > -1) {
        items.splice(index, 1);
      }
    }

    if (!userProfile.user || Config.HideCartAndCheckout) {
      index = _.findIndex(items, (item) => item.label == Languages.MyOrder);
      if (index > -1) {
        items.splice(index, 1);
      }
    }
    return items;
  };

  _onToggleDarkTheme = () => {
    this.props.toggleDarkTheme();
  };

  _handleSwitchNotification = (value) => {
    AsyncStorage.setItem("@notification", JSON.stringify(value), () => {
      this.setState({
        pushNotification: value,
      });
    });
  };

  _handlePress = (item) => {
    const { navigation } = this.props;
    const { routeName, isActionSheet } = item;

    if (routeName && !isActionSheet) {
      navigation.navigate(routeName, item.params);
    }

    if (isActionSheet) {
      this.currencyPicker.openModal();
    }
  };

  render() {
    const { userProfile, navigation, currency, changeCurrency } = this.props;
    const {clickedState} = this.state;
    const user = userProfile.user || {};
    const name = Tools.getName(user);
    const listItem = this._getListItem();
    const {
      theme: {
        colors: { background, lineColor },
        dark,
      },
    } = this.props;

    return (
      <View style={[styles.container, { backgroundColor: background }]}>
      {/*  <ImageBackground source={require('./../../images/body2.jpg')} style={{width: '100%', height: '100%'}}>

        <ScrollView ref="scrollView">
          <UserProfileHeader
            onLogin={() => navigation.navigate("LoginScreen")}
            onLogout={() =>
              navigation.navigate("LoginScreen", { isLogout: true })
            }
            user={{
              ...user,
              name,
            }}
          />

          {userProfile.user && (
            <View
              style={[styles.profileSection(dark), { borderColor: lineColor }]}>
              <Text style={styles.headerSection}>
                {Languages.AccountInformations.toUpperCase()}
              </Text>
              <UserProfileItem
                label={Languages.Name}
                onPress={this._handlePress}
                value={name}
              />
              <UserProfileItem label={Languages.Email} value={user.email} />
              //<UserProfileItem label={Languages.Address} value={user.address} />
            </View>
          )}

          <View
            style={[styles.profileSection(dark), { borderColor: lineColor }]}>
            {listItem.map((item, index) => {
              return (
                item && (
                  <UserProfileItem
                    icon
                    key={index.toString()}
                    onPress={() => this._handlePress(item)}
                    {...item}
                  />
                )
              );
            })}
          </View>
        </ScrollView>

        <ModalBox ref={(c) => (this.currencyPicker = c)}>
          <CurrencyPicker currency={currency} changeCurrency={changeCurrency} />
        </ModalBox>
        </ImageBackground>*/}
        <ImageBackground style={{width:'100%', height:'100%'}} source={require('./../../images/body2.jpg')}>
              <ScrollView
                style={{paddingLeft:width*0.05, paddingRight:width*0.05, backgroundColor:'transparent', marginTop:height*0.02,marginBottom:height*0.01, flexDirection:'column', height:'100%'}}
              >
              <Image source={{uri:'https://naipala.com/frontend-assets/images/profile.png'}}
                style={{width:width*0.4, height:width*0.4, alignSelf:'center', marginTop:height*0.05,}}
              />
              <Text style={{color:'#000',alignSelf:'center', marginTop:height*0.02, }}
            onPress={()=>this.setClickedState('editProfile')}

              >
                Edit Profile
                </Text>
                
                <TouchableOpacity style={{alignItems: 'center',
                  backgroundColor: '#212529',
                  padding: 4, marginTop:height*0.02, width:'80%', alignSelf:'center', }}>
                      <Text style={{color:'#fff',paddingRight:width*0.02, paddingLeft:width*0.02}}>
                LOG OUT
                </Text>
                </TouchableOpacity>
              <View style={{height:height*0.1, }}>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}
                  style={{backgroundColor:'transparent', height:height*0.25, paddingTop:5, alignSelf:'center', flexDirection:'row',}}
                  contentContainerStyle = {{justifyContent:'flex-start', alignItems:'center',}}
                >
                <TouchableOpacity style={{alignItems: 'center',
            backgroundColor: '#ffffff8c',
            padding: 4, borderRadius:1, }}
            onPress={()=>this.setClickedState('info')}
            >
                <Text style={{color:'#81302e',paddingRight:width*0.02, paddingLeft:width*0.02}}>
                Info
                </Text>
                </TouchableOpacity >
          <TouchableOpacity  style={{alignItems: 'center',
            backgroundColor: '#ffffff8c',
            padding: 4, borderRadius:1, marginLeft:10,}}
            onPress={()=>this.setClickedState('myHistory')}
            >
                <Text style={{color:'#81302e',paddingRight:width*0.02, paddingLeft:width*0.02}}>
                My history
                </Text>

                </TouchableOpacity>
                <TouchableOpacity style={{alignItems: 'center',
            backgroundColor: '#ffffff8c',
            padding: 4, borderRadius:1, marginLeft:10,}}
            onPress={()=>this.setClickedState('post')} 
            >
                <Text style={{color:'#81302e',paddingRight:width*0.02, paddingLeft:width*0.02}}>Post</Text></TouchableOpacity>
                <TouchableOpacity style={{alignItems: 'center',
            backgroundColor: '#ffffff8c',
            padding: 4, borderRadius:1, marginLeft:10,}}
            onPress={()=>this.setClickedState('applicants')}
            >
                <Text style={{color:'#81302e',paddingRight:width*0.02, paddingLeft:width*0.02}}>Applicants
                </Text>
                </TouchableOpacity>
                </ScrollView>
                </View>
                <View style={{width: '100%', borderBottomColor:'#fff', borderBottomWidth:1, alignSelf:'center'}}/>
                {clickedState==='info' &&
                  <View style={{flex: 1, flexDirection: 'column', height:height*0.28, marginTop:height*0.05}}>
                <View style={{width: '100%', height:'25%' , backgroundColor: '#00000008', justifyContent:'flex-end', alignItems:'center', flexDirection:'row'}}>
                <View style={{width:'90%'}}>
                  <Text style={{alignSelf:'center', fontSize:24, fontWeight:'500', }}>
                    My Info
                  </Text>
                </View>
                  <Image source={{uri:'https://cdn3.iconfinder.com/data/icons/block/32/box_edit-512.png'}}
                style={{height:'60%', width:'8%', marginRight:'4%' }} onPress={()=>this.setClickedState('editProfile')}/>
                </View>
                <View style={{width: '100%', height:'75%' ,  backgroundColor: '#ffffff08', padding:width*0.04,}}>
                  <Text style={{ fontSize:16, fontWeight:'500'}}>
                    Name:
                  </Text>
                  <Text style={{ fontSize:16,}}>
                    Shrawan Bishowkarma {'\n'}
                  </Text>
                  
                  <Text style={{ fontSize:16, fontWeight:'500'}}>
                    Email:
                  </Text>
                  <Text style={{ fontSize:16,}}>
                    sbk.shrawan@gmail.com
                  </Text>
                </View>
              </View>}
               <View style={{width: '100%', borderBottomColor:'#fff', borderBottomWidth:1, alignSelf:'center'}}/>
                {clickedState==='myHistory' && <MyHistory/>}
               <View style={{width: '100%', borderBottomColor:'#fff', borderBottomWidth:1, alignSelf:'center'}}/>
                { clickedState==='post' &&
                <View style={{flex: 1, flexDirection: 'column', marginTop:height*0.05}}>
                <View style={{width: '100%', height:height*0.08 , backgroundColor: '#00000008', justifyContent:'flex-end', alignItems:'center', flexDirection:'row'}}>
                <View style={{width:'90%'}}>
                  <Text style={{alignSelf:'center', fontSize:20, fontWeight:'500', }}>
                    My Posts
                  </Text>
                </View>
                <View style={{borderRadius:1, borderColor:'#000', borderWidth:1,}}>
                <TouchableOpacity style={{alignItems: 'center',
                  backgroundColor: 'transparent',
                  alignSelf:'center', }}
                  onPress={()=>this.setClickedState('addPost')}>
                      <Text style={{color:'#000',paddingRight:width*0.02, paddingLeft:width*0.02, fontSize:18}}>
                ADD + 
                </Text>
                </TouchableOpacity>
                </View>
                </View>
                {/*<View style={{width: '100%', height:'75%' ,  backgroundColor: '#ffffff08', padding:width*0.04,}}>
                  <Text style={{ fontSize:16, fontWeight:'500', alignSelf:'center'}}>
                    No services
                  </Text>
                </View>*/}
                  <View style={{flex: 1, flexDirection: 'column', marginTop:height*0.05}}>
                    <View style={{width: '100%', height:height*0.12 , backgroundColor: '#0000000d', justifyContent:'flex-start', alignItems:'flex-start', flexDirection:'column'}}>
                    <Text style={{alignSelf:'center', fontSize:24, fontWeight:'500', width:'100%', marginBottom:12 }}>
                    May 6, 2019
                    </Text>
                    <View style={{ flexDirection:'row',justifyContent:'space-between', alignItems:'flex-start', flex:1}}>
                    <Text style={{color:'#999', fontSize:22, fontWeight:'500', width:'75%'}}>Post 1
                    </Text>
                    <TouchableOpacity style={{alignItems: 'center',
                    backgroundColor: '#343a40',
                    paddingTop: 4,paddingRight: 8,paddingBottom: 4,paddingLeft: 8, borderRadius:6, width:'25%', justifyContent:'flex-end'}}>
                    <Text style={{color:'#fff',fontSize:12,}}>
                    Education
                    </Text>
                    </TouchableOpacity>
                    </View>
                    </View>
                    <View style={{width: '100%',  backgroundColor: '#d0bcbc0d',paddingTop:height*0.01,paddingBottom:height*0.01,}}>
                    <Text style={{ fontSize:16, fontWeight:'500'}}>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </Text>
                    <View style={{ flexDirection:'row',justifyContent:'space-between', alignItems:'flex-start', marginTop:height*0.02}}>
                    <TouchableOpacity style={{alignItems: 'center',
                    backgroundColor: '#6c757d',
                    paddingTop: 4,paddingRight: 8,paddingBottom: 4,paddingLeft: 8, borderRadius:6, width:'25%', justifyContent:'flex-end'}}>
                    <Text style={{color:'#fff',fontSize:12,}}>
                    Edit Post
                    </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{alignItems: 'center',
                      backgroundColor: '#343a40',
                      paddingTop: 4,paddingRight: 8,paddingBottom: 4,paddingLeft: 8, borderRadius:6, width:'30%', 
                      justifyContent:'flex-end'}}
                      onPress={()=>{
                        Alert.alert(
                          'Remove Post',
                          'Are you sure you want to remove the post?',
                          [
                            {
                              text: 'Cancel',
                              onPress: () => console.log('Cancel Pressed'),
                              style: 'cancel',
                            },
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                          ],
                          {cancelable: false},
                        )
                      }}
                    >
                    <Text style={{color:'#fff',fontSize:12,}}>
                    Remove Post
                    </Text>
                    </TouchableOpacity>
                    </View>
                    </View>
                    </View>
              </View>
                }

              {/*add post starts here */}
            { clickedState==='addPost' &&
              <View style={{flexDirection:'column', width:'100%', flex:1}}>
              <Text style={{color:'#000',paddingRight:width*0.02, paddingLeft:width*0.02, fontSize:18, height:20, alignSelf:'center'}}>
                Add Post Details
                </Text>
                 <View style={{width: '100%', borderBottomColor:'#fff', borderBottomWidth:1, marginTop:width*0.04}}/>
                  <View style={{width: '100%', height:'12%', flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:15}}>
                  <Text style={{
                    color:'#000',
                    fontSize:18,
                    fontWeight:'400',
                  }}>
                    Title
                  </Text>
                  <TextInput
                    style={{height: 30, width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
                    onChangeText={(text) => console.log({text})}
                    placeholder="Enter Title"
                  />
                  </View>
                  <View style={{width: '100%', height:'12%', flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:15}}>
                  <Text style={{
                    color:'#000',
                    fontSize:18,
                    fontWeight:'400',
                  }}>
                    Category
                  </Text>
                   <View style={{width: '100%', height:30, justifyContent:'center', backgroundColor:'#fff'}}
         >
                  <Picker
                    onValueChange={(itemValue, itemIndex) =>
                      console.log("itemValue-",itemValue)
                    }
                    mode='dropdown'
                    style={{height:'100%', width: '100%'}}
                    placeholder="Please select a category"
                  >
                    <Picker.Item label="Please select a category" value=""/>
                    <Picker.Item label="JOB" value="Nepal"/>
                    <Picker.Item label="ENTREPRENERUSHIP" value="China" />
                    <Picker.Item label="CATERIN" value="India" />
                    <Picker.Item label="FASHION" value="Australia" />
                    <Picker.Item label="EVENTS" value="USA" />
                  </Picker>
                  </View>
                  </View>
                  <View style={{width: '100%', height:'24%', flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:15}}>
                  <Text style={{
                    color:'#444',
                    fontSize:18,
                    fontWeight:'600',
                  }}>
                    Description
                  </Text>
                  <TextInput
                    style={{width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
                    onChangeText={(text) => console.log({text})}
                    multiline = {true}
                    numberOfLines = {6}
                    placeholder="Enter Description of your service"
                  />
                  </View>
                  <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                        <TouchableOpacity style={{ alignItems:'center',
                  backgroundColor: '#343a40',
                  paddingTop: 4,paddingBottom: 4,width:'30%', borderRadius:4, marginTop:height*0.05}}
                  onPress={()=>this.setClickedState('post')}

                  >
                      <Text style={{color:'#fff',fontSize:16,}}>
                      Back
                      </Text>
                      </TouchableOpacity>
                  <TouchableOpacity style={{ alignItems:'center',
            backgroundColor: '#343a40',
            paddingTop: 4,paddingBottom: 4,width:'30%', borderRadius:4, marginTop:height*0.05}}>
                <Text style={{color:'#fff',fontSize:16,}}>
                Add Post
                </Text>
                </TouchableOpacity>
                </View>
                </View>
            }
                {/* Edit profile here */}
                {clickedState==='editProfile' &&
                  <View style={{flexDirection:'column', height:height*1.2, width:'100%', flex:1}}>
              <Text style={{color:'#000',paddingRight:width*0.02, paddingLeft:width*0.02, fontSize:18, height:20, alignSelf:'center'}}>
                Edit Profile
                </Text>
                 <View style={{width: '100%', borderBottomColor:'#fff', borderBottomWidth:1, marginTop:width*0.04,}}/>
                  <View style={{width: '100%', flexDirection:'column', height:height*1.1, alignItems:'center', justifyContent:'space-around', marginTop:width*0.04}}>
                  <TextInput
                    style={{height: '6%', width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
                    onChangeText={(text) => console.log({text})}
                    placeholder="Enter Name"
                  />
                  <TextInput
                    style={{height: '6%', width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
                    onChangeText={(text) => console.log({text})}
                    placeholder="Address Line 1"
                  />
                  <TextInput
                    style={{height: '6%', width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
                    onChangeText={(text) => console.log({text})}
                    placeholder="Address Line 2 ( optional )"
                  />
                  <TextInput
                    style={{height: '6%', width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
                    onChangeText={(text) => console.log({text})}
                    placeholder="Suburb"
                  />
                  <TextInput
                    style={{height: '6%', width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
                    onChangeText={(text) => console.log({text})}
                    placeholder="City"
                  />
                  <View style={{width: '100%', height:'6%', justifyContent:'center', backgroundColor:'#fff'}}
                 >
                  <Picker
                    onValueChange={(itemValue, itemIndex) =>
                      console.log("itemValue-",itemValue)
                    }
                    mode='dropdown'
                    style={{height:'100%', width: '100%'}}
                    placeholder="Pick your amount"
                  >
                    <Picker.Item label="Choose Country" value=""/>
                    <Picker.Item label="Nepal" value="Nepal"/>
                    <Picker.Item label="China" value="China" />
                    <Picker.Item label="India" value="India" />
                    <Picker.Item label="Australia" value="Australia" />
                    <Picker.Item label="USA" value="USA" />
                  </Picker>
                  </View>
                  <View style={{width: '100%', height:'6%', justifyContent:'center', backgroundColor:'#fff'}}
                 >
                  <Picker
                    onValueChange={(itemValue, itemIndex) =>
                      console.log("itemValue-",itemValue)
                    }
                    mode='dropdown'
                    style={{height:'100%', width: '100%'}}
                    placeholder="Pick your amount"
                  >
                    <Picker.Item label="Australian Capital" value=""/>
                    <Picker.Item label="Nepal" value="Nepal"/>
                    <Picker.Item label="China" value="China" />
                    <Picker.Item label="India" value="India" />
                    <Picker.Item label="Australia" value="Australia" />
                    <Picker.Item label="USA" value="USA" />
                  </Picker>
                  </View>
                  <TextInput
                    style={{height: '6%', width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
                    onChangeText={(text) => console.log({text})}
                    placeholder="Postcode"
                  />
                  {/*<TextInput
                    style={{height: '8%', width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
                    onChangeText={(text) => console.log({text})}
                    placeholder="Date of Birth"
                  />*/}
                  <DatePicker
                style={{width: '100%', height: '6%',backgroundColor:'#fff', alignSelf: 'center'}}
                mode="date"
                placeholder="Date of Birth"
                format="YYYY-MM-DD"
                minDate="2016-05-01"
                maxDate="2020-06-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={false}
                customStyles={{
                  dateInput: {
                    height:'100%'
                  }
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(date) => {console.log("date---")}}
              />
                  <TextInput
                    style={{height: '6%', width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
                    onChangeText={(text) => console.log({text})}
                    placeholder="Phone"
                  />
                  <View style={{height: '5%', width:'100%', flexDirection:'row', justifyContent:'flex-end', alignItems:'flex-end'}}>
                  <TouchableOpacity style={{ alignItems:'center',
            backgroundColor: '#343a40',
            paddingTop: 4,paddingBottom: 4,width:'30%', borderRadius:4, }}>
                <Text style={{color:'#fff',fontSize:16,}}>
                SAVE
                </Text>
                </TouchableOpacity>
                </View>
                          </View>
                </View>}
              {/*applicants*/}
                {clickedState==='applicants' && (
                    <View
                              style={{
                                flexDirection: 'column',
                                marginTop: 20,
                                alignItems: 'center',
                              }}>
                              <View
                                style={{
                                  width: '100%',
                                  height: height * 0.06,
                                  justifyContent: 'center',
                                  backgroundColor: '#fff',
                                  borderRadius: 4,
                                }}>
                                <Picker
                                  onValueChange={(itemValue, itemIndex) =>
                                    console.log('itemValue-', itemValue)
                                  }
                                  mode="dropdown"
                                  style={{ height: '100%', width: '100%' }}
                                  placeholder="Pick your amount">
                                  <Picker.Item label="Select Category" value="" />
                                  <Picker.Item label="Nepal" value="Nepal" />
                                  <Picker.Item label="China" value="China" />
                                  <Picker.Item label="India" value="India" />
                                  <Picker.Item label="Australia" value="Australia" />
                                  <Picker.Item label="USA" value="USA" />
                                </Picker>
                              </View>
                              <ScrollView style={{ width: '100%', marginTop: 20 }}>
                                {/*hahaha*/}
                                <View
                                  style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    backgroundColor: '#ffffff73',
                                    justifyContent: 'space-around',
                                    borderBottomWidth: 1,
                                    borderBottomColor: '#00000033',
                                  }}>
                                  <Text
                                    style={{
                                      padding: width * 0.03,
                                    }}>
                                    12 minutes ago
                                  </Text>
                                  <View
                                    style={{
                                      flex: 1,
                                      flexDirection: 'row',
                                      alignItems: 'flex-start',
                                      justifyContent: 'space-around',
                                    }}>
                                    <Image
                                      source={{
                                        uri:
                                          'https://naipala.projectincube.com/frontend-assets/images/profile.png',
                                      }}
                                      style={{ width: width * 0.24, height: width * 0.24 }}
                                    />
                                    <View
                                      style={{
                                        width: width * 0.5,
                                      }}>
                                      <Text>
                                        <Text style={{ fontWeight: '500', fontSize: 16 }}>
                                          Name:{' '}
                                        </Text>
                                        <Text style={{ fontSize: 15 }}>Shrawan Bishowkarma</Text>
                                      </Text>
                                      <Text>
                                        <Text style={{ fontWeight: '500', fontSize: 16 }}>
                                          Title:{' '}
                                        </Text>
                                        <Text style={{ fontSize: 15 }}>Post 100</Text>
                                      </Text>
                                      <Text>
                                        <Text style={{ fontWeight: '500', fontSize: 16 }}>
                                          Category:{' '}
                                        </Text>
                                        <Text style={{ fontSize: 15 }}>Category 1</Text>
                                      </Text>
                                      <View
                                        style={{
                                          width: '100%',
                                          alignItems: 'flex-end',
                                          justifyContent: 'center',
                                          marginBottom: 10,
                                          marginTop: 10,
                                        }}>
                                        <Text style={{ fontSize: 14, color: 'blue' }}>
                                          View Details
                                        </Text>
                                      </View>
                                    </View>
                                  </View>
                                </View>
                                {/*hahaha*/}
                                <View
                                  style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    backgroundColor: '#ffffff73',
                                    justifyContent: 'space-around',
                                    borderBottomWidth: 1,
                                    borderBottomColor: '#00000033',
                                  }}>
                                  <Text
                                    style={{
                                      padding: width * 0.03,
                                    }}>
                                    12 minutes ago
                                  </Text>
                                  <View
                                    style={{
                                      flex: 1,
                                      flexDirection: 'row',
                                      alignItems: 'flex-start',
                                      justifyContent: 'space-around',
                                    }}>
                                    <Image
                                      source={{
                                        uri:
                                          'https://naipala.projectincube.com/frontend-assets/images/profile.png',
                                      }}
                                      style={{ width: width * 0.24, height: width * 0.24 }}
                                    />
                                    <View
                                      style={{
                                        width: width * 0.5,
                                      }}>
                                      <Text>
                                        <Text style={{ fontWeight: '500', fontSize: 16 }}>
                                          Name:{' '}
                                        </Text>
                                        <Text style={{ fontSize: 15 }}>Shrawan Bishowkarma</Text>
                                      </Text>
                                      <Text>
                                        <Text style={{ fontWeight: '500', fontSize: 16 }}>
                                          Title:{' '}
                                        </Text>
                                        <Text style={{ fontSize: 15 }}>Post 100</Text>
                                      </Text>
                                      <Text>
                                        <Text style={{ fontWeight: '500', fontSize: 16 }}>
                                          Category:{' '}
                                        </Text>
                                        <Text style={{ fontSize: 15 }}>Category 1</Text>
                                      </Text>
                                    </View>
                                  </View>
                                  <View
                                    style={{
                                      width: '100%',
                                      flexDirection: 'column',
                                      alignItems: 'flex-start',
                                      justifyContent: 'space-between',
                                      padding: width * 0.03,
                                    }}>
                                    <Text>
                                        <Text style={{ fontWeight: '500', fontSize: 16 }}>
                                          Email:{' '}
                                        </Text>
                                        <Text style={{ fontSize: 15 }}>abacsd.dafs@gmail.com</Text>
                                      </Text>
                                      <Text>
                                        <Text style={{ fontWeight: '500', fontSize: 16 }}>
                                          Phone:{' '}
                                        </Text>
                                        <Text style={{ fontSize: 15 }}>9841472728</Text>
                                      </Text>
                                      <Text>
                                        <Text style={{ fontWeight: '500', fontSize: 16 }}>
                                          Message:{' '}
                                        </Text>
                                        <Text style={{ fontSize: 15 }}>Hello i want to apply in your post Hello i want to apply in your post</Text>
                                      </Text>
                                    <View
                                        style={{
                                          width: '100%',
                                          alignItems: 'flex-end',
                                          justifyContent: 'center',
                                        }}>
                                        <Text style={{ fontSize: 14, color: 'blue' }}>
                                          Hide Details
                                        </Text>
                                      </View>
                                  </View>
                                </View>
                              </ScrollView>
                            </View>
                  )
                }
                </ScrollView>
                </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = ({ user, language, currency, wishList, app }) => ({
  userProfile: user,
  language,
  currency,
  wishListTotal: wishList.wishListItems.length,
  isDarkTheme: app.isDarkTheme,
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions: CurrencyActions } = require("@redux/CurrencyRedux");
  const { toggleDarkTheme } = require("@redux/AppRedux");
  return {
    ...ownProps,
    ...stateProps,
    changeCurrency: (currency) =>
      CurrencyActions.changeCurrency(dispatch, currency),
    toggleDarkTheme: () => {
      dispatch(toggleDarkTheme());
    },
  };
}

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(withTheme(UserProfile));

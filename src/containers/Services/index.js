/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Languages } from "@app/Omni";
import EvilIcons from "@expo/vector-icons/EvilIcons";
import styles from "./styles";
import {FlatList, Text, View, StyleSheet, ScrollView,TextInput, Dimensions, TouchableOpacity, Image,ImageBackground, } from 'react-native';
// import { userInfo } from "os";
// import { FlatList } from "react-native-gesture-handler";
const {height, width} = Dimensions.get('window');
import ApplyForServices from './ApplyForServices';
class ServicesScreen extends PureComponent {
  static propTypes = {
    onBack: PropTypes.func,
  };

  constructor(props) {
    super(props);

    // const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      // dataSource: ds.cloneWithRows(props.wishListItems),
      services: [],
      serviceItems:[],
      serviceCategoryIndex:0,
      selectedOption:'events',
      applyForServices:false,
    };

  }

  updateSelectedOptions=(index)=>{

    
    this.setState({
      serviceItems:this.state.services[index].data.data,
      serviceCategoryIndex:index
    });
  }

  updateApplyForServices=()=>{
    this.setState((prevstate)=>({
      applyForServices: !prevstate.applyForServices,
    }));
  }
 
   loadMoreServiceItems=async()=>{
     var updated_at=this.state.serviceItems[this.state.serviceItems.length-1].updated_at;
     var id=this.state.serviceCategoryIndex;
     if(this.state.serviceItems.length>4){
    fetch('http://naipala.projectincube.com/api/services?updated_at='+encodeURIComponent(updated_at)+'&id='+encodeURIComponent(id), {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
     
    }).then((response) => response.json())
    .then((responseJson) => {
      // console.log(responseJson,'paginatedresponse');
      // console.log(this.state.services[this.state.serviceCategoryIndex].data.data.concat(responseJson))
      
      updatedServices=this.state.services;
      updatedServices[this.state.serviceCategoryIndex].data.data=updatedServices[this.state.serviceCategoryIndex].data.data.concat(responseJson);
      updatedServiceItems=this.state.serviceItems.concat(responseJson);

      // console.log(updatedServiceItems,'updatesServiceItem');
       this.setState({
        services:updatedServices,
        serviceItems: updatedServiceItems
      });
    })
    .catch((error) => {
      console.error(error);
    });

  }
  }

  fetchServices=async()=>{
    const response= await fetch('http://naipala.projectincube.com/api/services', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      
    });
    const json=await response.json();
    this.setState({
      services:json
      
    });
    if(json.length>0)
    {
      this.setState({
        serviceItems:json[0].data.data
      })
    }
    console.log(this.state.services);
    console.log(this.state.serviceItems);

  

  }
  componentWillMount=async()=>{
    this.fetchServices();
  }
  renderServiceCategories=()=>{
   
   
  }
   isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };
  

  render() {

    const {
      selectedOption,
      applyForServices,
    } = this.state;
   
    return (
       <ImageBackground style={{width:'100%', height:'100%'}} source={require("@images/body2.jpg")}>
      <View
        style={{height:'96%', paddingLeft:width*0.05, paddingRight:width*0.05, backgroundColor:'transparent', marginTop:height*0.02,marginBottom:height*0.1,}}
      >
      <Text style={{fontSize:28, textAlign:'center', fontWeight:'500', color:'#81302e'}}>
            Services
      </Text>
      <View style={{height:'6%', marginTop:height*0.05, }}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={true}
          style={{backgroundColor:'transparent', height:height*0.3, paddingBottom:5,paddingTop:5,  flexDirection:'row',}}
          contentContainerStyle={{justifyContent:'flex-start', alignItems:'center',}}
          
        >
  {/* <FlatList 
    data={this.state.services}
    renderItem={({item}) => <Text>asd</Text>}
  /> */}
          {this.state.services.map( (service,index)=> (
                  <TouchableOpacity style={{alignItems: 'center',
                  backgroundColor: '#ffffff8c',
                  padding: 4, borderRadius:1, marginLeft:10,}}
                  onPress={()=>{
                    this.updateSelectedOptions(index)
                  }}
                  >
                <Text style={{color:'#81302e',paddingRight:width*0.025, paddingLeft:width*0.025}}>{service.service.title}</Text>
            </TouchableOpacity>
            ))
            
          }
  
  
        </ScrollView>
        </View>
        <ScrollView style={{flexDirection:'column', height:'90%'}} 
          onScroll={({nativeEvent}) => {
            if (this.isCloseToBottom(nativeEvent)) {
              this.loadMoreServiceItems();
            }
          }}
        >

          {
            !applyForServices &&
            <View style={{flexDirection: 'column', marginTop:height*0.05, marginBottom:height*0.02}}>
            {this.state.serviceItems.map( (item)=> (
              
              <View style={{marginBottom:20}}>
            <View style={{width: '100%', height: height*0.15, backgroundColor: '#0000000d', flexDirection:'column',}}>
              <Text style={{color:'#999', fontSize:20, marginTop:width*0.05, marginLeft:'5%'}}>Apr 22, 2019</Text>
              <View style={{marginLeft:'5%',marginRight:'5%', flexDirection:'row',justifyContent:'space-between', alignItems:'center', flex:1}}>
                <Text style={{color:'#81304D', fontSize:22, fontWeight:'500',}}>{item.title}</Text>
                <TouchableOpacity style={{alignItems: 'center',
                  backgroundColor: '#343a40',
                  paddingTop: 4,paddingRight: 8,paddingBottom: 4,paddingLeft: 8, borderRadius:6, }}>
              <Text style={{color:'#fff',fontSize:12,}}>
              {item.service_category.title}
              </Text>
              </TouchableOpacity>
              </View>
            
             </View>
            <View style={{ backgroundColor:'#ffffff8c' }}>
            <View style={{flexDirection:'column'}}>
            <View style={{width: '100%'}}>
              <Text style={{
                    fontSize:16, 
                    marginBottom:width*0.05,
                    whitespace:"pre-wrap"}
                  }
                  >
                 {item.description}
                </Text>
          
              </View>

              <View style={{width: '100%', flexDirection:'row',justifyContent:'space-between', alignItems:'center'}}>
                <Image  borderRadius={50} source={{uri: item.user.image!='' ?'https://naipala.com/profile_images/'+item.user.image:'https://naipala.com/frontend-assets/images/profile.png' }} style={{width:'25%',height:width*0.2}}/>
                      <Text style={styles.paragraph}>
                      {item.user.name}</Text>
                      <TouchableOpacity style={{alignItems: 'center',
                        backgroundColor: '#343a40',
                        padding: 4, borderRadius:10, width:'25%', }}
                        onPress={this.updateApplyForServices}
                      >
                    <Text style={{color:'#fff',fontSize:16,}}>
                    APPLY
                    </Text>
                    </TouchableOpacity>
              </View>
            </View>
            </View>
            </View>
            
            
           
            ))}
          </View>}
          {
            applyForServices && <ApplyForServices updateApplyForServices={this.updateApplyForServices}/>
          }
        </ScrollView>
        </View>
        </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    wishListItems: state.wishList.wishListItems,
    cartItems: state.carts.cartItems,
  };
};
export default connect(mapStateToProps)(ServicesScreen);

import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  WebView,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Dimensions,
  ImageBackground,
} from 'react-native';
import { Icon } from 'react-native-elements';
// import { WebView } from 'react-native-webview';
const { height, width } = Dimensions.get('window');
import { DocumentPicker } from 'expo';
export default class ApplyForServices extends React.Component {
    _pickDocument = async () => {
	    let result = await DocumentPicker.getDocumentAsync({});
		  alert(result.uri);
      console.log(result);
	}

  render() {
    const {
      updateApplyForServices
    } = this.props;
    return (
      <ImageBackground
        style={{ height: '100%', width: '100%' }}
        source={require("@images/background.png")}>
        <ScrollView>
          <View style={{ flex: 1, padding: 10, backgroundColor: 'transparent' }}>
            <View
              style={{
                width: '100%',
                height: height * 0.06,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{ fontSize: 20 }}>Provide Additional Info?</Text>
            </View>
            <View
              style={{
                borderBottomColor: '#fff',
                borderBottomWidth: 2,
                width: '100%',
              }}
            />

            <View
              style={{
                height: height * 0.2,
                alignItems: 'flex-end',
                justifyContent: 'space-around',
                marginTop:height*0.02,
              }}>
              <TextInput
                style={{
                  backgroundColor: '#e8e8e8',
                  border: 'none',
                  width: '100%',
                  color: 'black',
                  padding: 20,
                  height: '100%',
                  marginTop: height * 0.02,
                  borderRadius:4,
                  
                }}
                placeholderTextColor='#99a0a6'
                // onChangeText={(text) => this.setState({text})}
                placeholder="Leave a message?"
                multiline
              />
            </View>
            <View
              style={{
                width: '100%',
                height: height * 0.12,
                justifyContent: 'space-around',
                alignItems: 'flex-start',
                marginTop:height*0.02,
              }}>
              <Text style={{ fontSize: 16, marginBottom:height*0.02, }}>
                Leave your CV(pdf,doc,rtf)? (optional)
              </Text>
              <TouchableOpacity
                activeOpacity={0.5}
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  borderColor: '#00000026',
                  borderWidth: 1,
                  backgroundColor: '#e8e8e8',
                  width: '100%',
                  padding:4,
                  borderRadius:4,
                }}
                onPress={this._pickDocument}
                >
                <Image
                  source={{
                    uri:
                      'https://cdn3.iconfinder.com/data/icons/alicons/32/multiple_files-512.png',
                  }}
                  style={styles.ImageIconStyle}
                />
                <Text style={{ fontSize: 16 }}>Choose a file</Text>
                <Text style={{ fontSize: 16 }}>(No File Choosen)</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop:height*0.01 }}>
              <TouchableOpacity
                style={[styles.addReviewButton, { marginTop: 10 }]}>
                <Text
                  style={{ color: '#fff', fontSize: 14, fontWeight: '500' }}>
                  SUBMIT
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.addReviewButton, { marginTop: 10 }]}
                onPress={updateApplyForServices}
              >
                <Text
                  style={{ color: '#fff', fontSize: 14, fontWeight: '500' }}>
                  CANCEL
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  addReviewButton: {
    backgroundColor: '#000',
    borderRadius: 2,
    borderWidth: 2,
    borderColor: '#23272b',
    justifyContent: 'center',
    height: width * 0.1,
    width: width * 0.3,
    alignItems: 'center',
  },
  ImageIconStyle: {
    height: height * 0.04,
    width: height * 0.04,
    resizeMode: 'stretch',
  },
  logo: {
    height: 128,
    width: 128,
  },
});

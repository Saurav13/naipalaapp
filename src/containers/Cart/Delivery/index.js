/** @format */

import React, { PureComponent } from "react";
import { Text, View, AsyncStorage, ScrollView, Dimensions, TouchableOpacity, Image, TextInput, ImageBackground,Picker } from "react-native";
import { connect } from "react-redux";
import Tcomb from "tcomb-form-native";
import { cloneDeep } from "lodash";
import { TextInputMask } from "react-native-masked-text";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import CountryPicker from "react-native-country-picker-modal";

import Buttons from "@cart/Buttons";
import { ShippingMethod } from "@components";
import { Config, Validator, Languages, withTheme, Styles } from "@common";
import { toast } from "@app/Omni";
import css from "@cart/styles";
import styles from "./styles";

const Form = Tcomb.form.Form;

const customStyle = cloneDeep(Tcomb.form.Form.stylesheet);
const customInputStyle = cloneDeep(Tcomb.form.Form.stylesheet);
const {height, width} = Dimensions.get('window');

class Delivery extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: {
        first_name: "",
        last_name: "",
        address_1: "",
        state: "",
        postcode: "",
        country: "",
        email: "",
        phone: "",
        note: "",
      },
      cca2: "EN",
    };

    this.initFormValues();
  }

  componentDidMount() {
    const { getShippingMethod } = this.props;

    this.fetchCustomer(this.props);
    getShippingMethod(Config.shipping.zoneId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user != this.props.user) {
      this.fetchCustomer(nextProps);
    }
  }

  _getCustomStyle = () => {
    const {
      theme: {
        colors: { text },
      },
    } = this.props;
    // Customize Form Stylesheet
    customStyle.textbox.normal = {
      ...customStyle.textbox.normal,
      height: 150,
      marginBottom: 200,
      color: text,
    };
    customStyle.controlLabel.normal = {
      ...customStyle.controlLabel.normal,
      fontSize: 15,
      color: text,
    };

    return customStyle;
  };

  _getCustomInputStyle = () => {
    const {
      theme: {
        colors: { text },
      },
    } = this.props;

    customInputStyle.controlLabel.normal = {
      ...customInputStyle.controlLabel.normal,
      fontSize: 15,
      color: text,
    };
    customInputStyle.textbox.normal = {
      ...customInputStyle.textbox.normal,
      color: text,
      width: Styles.width / 2,
    };
    customInputStyle.textbox.error = {
      ...customInputStyle.textbox.normal,
      color: text,
      width: Styles.width / 2,
    };
    customInputStyle.formGroup.normal = {
      ...customInputStyle.formGroup.normal,
      flexDirection: "row",
      flexWrap: "wrap",
      alignItems: "center",
      justifyContent: "space-between",
    };
    customInputStyle.formGroup.error = {
      ...customInputStyle.formGroup.normal,
      flexDirection: "row",
      flexWrap: "wrap",
      alignItems: "center",
      justifyContent: "space-between",
    };
    customInputStyle.errorBlock = {
      ...customInputStyle.errorBlock,
      width: Styles.width,
    };

    return customInputStyle;
  };

  onChange = (value) => this.setState({ value });

  onPress = () => this.refs.form.getValue();

  initFormValues = () => {
    const {
      theme: {
        colors: { placeholder },
      },
    } = this.props;
    // const countries = this.props.countries;
    // override the validate method of Tcomb lib for multi validate requirement.
    // const Countries = Tcomb.enums(countries);
    const Email = Tcomb.refinement(
      Tcomb.String,
      (s) => Validator.checkEmail(s) === undefined
    );
    Email.getValidationErrorMessage = (s) => Validator.checkEmail(s);
    const Phone = Tcomb.refinement(
      Tcomb.String,
      (s) => Validator.checkPhone(s) === undefined
    );
    Phone.getValidationErrorMessage = (s) => Validator.checkPhone(s);

    // define customer form
    this.Customer = Tcomb.struct({
      first_name: Tcomb.String,
      last_name: Tcomb.String,
      address_1: Tcomb.String,
      ...(Config.DefaultCountry.hideCountryList
        ? {}
        : { country: Tcomb.String }),
      state: Tcomb.String,
      city: Tcomb.String,
      postcode: Tcomb.String,
      email: Email,
      phone: Tcomb.String,
      note: Tcomb.maybe(Tcomb.String), // maybe = optional
    });

    // form options
    this.options = {
      auto: "none", // we have labels and placeholders as option here (in Engrish, ofcourse).
      // stylesheet: css,
      fields: {
        first_name: {
          label: Languages.FirstName,
          placeholder: Languages.TypeFirstName,
          error: Languages.EmptyError, // for simple empty error warning.
          underlineColorAndroid: "transparent",
          stylesheet: this._getCustomInputStyle(),
          placeholderTextColor: placeholder,
        },
        last_name: {
          label: Languages.LastName,
          placeholder: Languages.TypeLastName,
          error: Languages.EmptyError,
          underlineColorAndroid: "transparent",
          stylesheet: this._getCustomInputStyle(),
          placeholderTextColor: placeholder,
        },
        address_1: {
          label: Languages.Address,
          placeholder: Languages.TypeAddress,
          error: Languages.EmptyError,
          underlineColorAndroid: "transparent",
          stylesheet: this._getCustomInputStyle(),
          placeholderTextColor: placeholder,
        },
        ...(Config.DefaultCountry.hideCountryList
          ? {}
          : {
              country: {
                label: Languages.TypeCountry,
                placeholder: Languages.Country,
                error: Languages.NotSelectedError,
                stylesheet: this._getCustomInputStyle(),
                template: this.renderCountry,
                placeholderTextColor: placeholder,
              },
            }),
        state: {
          label: Languages.State,
          placeholder: Languages.TypeState,
          error: Languages.EmptyError,
          underlineColorAndroid: "transparent",
          stylesheet: this._getCustomInputStyle(),
          autoCorrect: false,
          placeholderTextColor: placeholder,
        },
        city: {
          label: Languages.City,
          placeholder: Languages.TypeCity,
          error: Languages.EmptyError,
          underlineColorAndroid: "transparent",
          stylesheet: this._getCustomInputStyle(),
          autoCorrect: false,
          placeholderTextColor: placeholder,
        },
        postcode: {
          label: Languages.Postcode,
          placeholder: Languages.TypePostcode,
          error: Languages.EmptyError,
          underlineColorAndroid: "transparent",
          stylesheet: this._getCustomInputStyle(),
          autoCorrect: false,
          placeholderTextColor: placeholder,
        },
        email: {
          label: Languages.Email,
          placeholder: Languages.TypeEmail,
          underlineColorAndroid: "transparent",
          stylesheet: this._getCustomInputStyle(),
          autoCorrect: false,
          placeholderTextColor: placeholder,
        },
        phone: {
          label: Languages.Phone,
          placeholder: Languages.TypePhone,
          underlineColorAndroid: "transparent",
          error: Languages.EmptyError,
          stylesheet: this._getCustomInputStyle(),
          template: this.renderPhoneInput,
          autoCorrect: false,
          placeholderTextColor: placeholder,
        },
        note: {
          label: Languages.Note,
          placeholder: Languages.TypeNote,
          underlineColorAndroid: "transparent",
          multiline: true,
          stylesheet: this._getCustomStyle(),
          autoCorrect: false,
          placeholderTextColor: placeholder,
        },
      },
    };
  };

  renderPhoneInput = (locals) => {
    const {
      theme: {
        colors: { placeholder },
      },
    } = this.props;
    const stylesheet = locals.stylesheet;
    let formGroupStyle = stylesheet.formGroup.normal;
    let controlLabelStyle = stylesheet.controlLabel.normal;
    let textboxStyle = stylesheet.textbox.normal;
    let helpBlockStyle = stylesheet.helpBlock.normal;
    const errorBlockStyle = stylesheet.errorBlock;

    if (locals.hasError) {
      formGroupStyle = stylesheet.formGroup.error;
      controlLabelStyle = stylesheet.controlLabel.error;
      textboxStyle = stylesheet.textbox.error;
      helpBlockStyle = stylesheet.helpBlock.error;
    }

    const label = locals.label ? (
      <Text style={controlLabelStyle}>{locals.label}</Text>
    ) : null;
    const help = locals.help ? (
      <Text style={helpBlockStyle}>{locals.help}</Text>
    ) : null;
    const error =
      locals.hasError && locals.error ? (
        <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>
          {locals.error}
        </Text>
      ) : null;

    return (
      <View style={formGroupStyle}>
        {label}
        <TextInputMask
          type="cel-phone"
          style={textboxStyle}
          onChangeText={(value) => locals.onChange(value)}
          onChange={locals.onChangeNative}
          placeholder={locals.placeholder}
          value={locals.value}
          placeholderTextColor={placeholder}
        />
        {help}
        {error}
      </View>
    );
  };

  renderCountry = (locals) => {
    const {
      theme: {
        colors: { placeholder },
      },
    } = this.props;
    const stylesheet = locals.stylesheet;
    let formGroupStyle = stylesheet.formGroup.normal;
    let controlLabelStyle = stylesheet.controlLabel.normal;
    let textboxStyle = stylesheet.textbox.normal;
    let helpBlockStyle = stylesheet.helpBlock.normal;
    const errorBlockStyle = stylesheet.errorBlock;

    if (locals.hasError) {
      formGroupStyle = stylesheet.formGroup.error;
      controlLabelStyle = stylesheet.controlLabel.error;
      textboxStyle = stylesheet.textbox.error;
      helpBlockStyle = stylesheet.helpBlock.error;
    }

    const label = locals.label ? (
      <Text style={controlLabelStyle}>{locals.label}</Text>
    ) : null;
    const help = locals.help ? (
      <Text style={helpBlockStyle}>{locals.help}</Text>
    ) : null;
    const error =
      locals.hasError && locals.error ? (
        <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>
          {locals.error}
        </Text>
      ) : null;

    return (
      <View style={formGroupStyle}>
        {label}
        <CountryPicker
          onChange={(value) => {
            this.setState({ cca2: value.cca2 });
            locals.onChange(value.name);
          }}
          cca2={this.state.cca2}
          filterable>
          <Text
            style={[
              textboxStyle,
              locals.value == "" && { color: placeholder },
            ]}>
            {locals.value == "" ? locals.placeholder : locals.value}
          </Text>
        </CountryPicker>
        {help}
        {error}
      </View>
    );
  };

  fetchCustomer = async (props) => {
    const { selectedAddress } = props;
    const { user: customer } = props.user;

    let value = selectedAddress;
    if (!selectedAddress && customer) {
      value = {
        first_name:
          customer.billing.first_name == ""
            ? customer.first_name
            : customer.billing.first_name,
        last_name:
          customer.billing.last_name == ""
            ? customer.last_name
            : customer.billing.last_name,
        email:
          customer.email.first_name == ""
            ? customer.email
            : customer.billing.email,
        address_1: customer.billing.address_1,
        city: customer.billing.city,
        state: customer.billing.state,
        postcode: customer.billing.postcode,
        country: customer.billing.country,
        phone: customer.billing.phone,
      };
    }

    this.setState({ value });
  };

  validateCustomer = async (customerInfo) => {
    await this.props.validateCustomerInfo(customerInfo);
    if (this.props.type === "INVALIDATE_CUSTOMER_INFO") {
      toast(this.props.message);
      return false;
    }
    this.props.onNext();
  };

  saveUserData = async (userInfo) => {
    this.props.updateSelectedAddress(userInfo);
    try {
      await AsyncStorage.setItem("@userInfo", JSON.stringify(userInfo));
    } catch (error) {

    }
  };

  selectShippingMethod = (item) => {
    this.props.selectShippingMethod(item);
  };

  nextStep = () => {
    const value = this.refs.form.getValue();
    if (value) {
      let country = "";
      if (Config.DefaultCountry.hideCountryList) {
        country = Config.DefaultCountry.countryCode.toUpperCase();
      } else {
        // Woocommerce only using cca2 to checkout
        country = this.state.cca2;
      }
      // if validation fails, value will be null
      this.props.onNext({ ...this.state.value, country });

      // save user info for next use
      this.saveUserData({ ...this.state.value, country });
    }
  };

  render() {
    const { shippings, shippingMethod } = this.props;
    const isShippingEmpty = typeof shippingMethod.id === "undefined";
    const {
      theme: {
        colors: { text },
      },
    } = this.props;

    return (
      <View style={styles.container}>
        <ImageBackground style={{width:'100%', height:'100%'}} source={require("@images/body2.jpg")}>
        <ScrollView
          style={{backgroundColor:'transparent', marginTop:height*0.02,marginBottom:height*0.1, flexDirection:'column' ,flex:1,}}
        >
          {/* Edit profile here */}
          <View style={{flexDirection:'column', width:'100%', flex:1, justifyContent:'flex-start'}}>
          <View style={{backgroundColor:'#ffffff33', paddingTop:height*0.025,paddingBottom:height*0.025, paddingLeft:width*0.05,paddingRight:width*0.05,width:width}}>
          <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'flex-start', height:height*0.1,}}>
          <Text style={{color:'#000',fontSize:20, width:width*0.5}}>
          Customer Information
          </Text>
          <Text style={{color:'#000',fontSize:18, width:width*0.4}}>
          Not you? Logout
          </Text>
          </View>
          <Text style={{color:'#000',fontSize:18, width:width}}>
          Logged In as ShrawanBk
          </Text>
          </View>
          <View style={{backgroundColor:'#ffffff33', paddingTop:height*0.025,paddingBottom:height*0.025, paddingLeft:width*0.05,paddingRight:width*0.05, marginTop:height*0.06, flexDirection:'column'}}>
        <Text style={{color:'#000',paddingRight:width*0.02, paddingLeft:width*0.02, fontSize:18, height:height*0.06,}}>
          Shipping Address
          </Text>
            <View style={{width: '100%', flexDirection:'column', height:height*1.1, alignItems:'center', justifyContent:'space-between',}}>
            <TextInput
              style={{height: height*0.06, width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
              onChangeText={(text) => console.log({text})}
              placeholder="Company Name ( Optional )"
            />
            <TextInput
              style={{height: height*0.06, width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
              onChangeText={(text) => console.log({text})}
              placeholder="Address Line 1"
            />
            <TextInput
              style={{height: height*0.06, width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
              onChangeText={(text) => console.log({text})}
              placeholder="Address Line 2 ( optional )"
            />
            <TextInput
              style={{height: height*0.06, width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
              onChangeText={(text) => console.log({text})}
              placeholder="Suburb"
            />
            <TextInput
              style={{height: height*0.06, width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
              onChangeText={(text) => console.log({text})}
              placeholder="City"
            />
            <View style={{width: '100%', height:height*0.06, justifyContent:'center', backgroundColor:'#fff'}}
           >
            <Picker
              onValueChange={(itemValue, itemIndex) =>
                console.log("itemValue-",itemValue)
              }
              mode='dropdown'
              style={{height:'100%', width: '100%'}}
              placeholder="Pick your amount"
            >
              <Picker.Item label="Choose Country" value=""/>
              <Picker.Item label="Nepal" value="Nepal"/>
              <Picker.Item label="China" value="China" />
              <Picker.Item label="India" value="India" />
              <Picker.Item label="Australia" value="Australia" />
              <Picker.Item label="USA" value="USA" />
            </Picker>
            </View>
            <View style={{width: '100%', height:height*0.06, justifyContent:'center', backgroundColor:'#fff'}}
           >
            <Picker
              onValueChange={(itemValue, itemIndex) =>
                console.log("itemValue-",itemValue)
              }
              mode='dropdown'
              style={{height:'100%', width: '100%'}}
              placeholder="Pick your amount"
            >
              <Picker.Item label="Australian Capital" value=""/>
              <Picker.Item label="Nepal" value="Nepal"/>
              <Picker.Item label="China" value="China" />
              <Picker.Item label="India" value="India" />
              <Picker.Item label="Australia" value="Australia" />
              <Picker.Item label="USA" value="USA" />
            </Picker>
            </View>
            <TextInput
              style={{height: height*0.06, width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
              onChangeText={(text) => console.log({text})}
              placeholder="Postcode"
            />
            {/*<TextInput
              style={{height: '8%', width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
              onChangeText={(text) => console.log({text})}
              placeholder="Date of Birth"
            />
            <DatePicker
          style={{width: '100%', height: '6%',backgroundColor:'#fff', alignSelf: 'center'}}
          mode="date"
          placeholder="Date of Birth"
          format="YYYY-MM-DD"
          minDate="2016-05-01"
          maxDate="2020-06-01"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          showIcon={false}
          customStyles={{
            dateInput: {
              height:'100%'
            }
            // ... You can check the source to find the other keys.
          }}
          onDateChange={(date) => {console.log("date---")}}
        />*/}
            <TextInput
              style={{height: height*0.06, width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
              onChangeText={(text) => console.log({text})}
              placeholder="Phone"
            />
            <View style={{height: '5%', width:'100%', flexDirection:'row', justifyContent:'flex-start', alignItems:'flex-start'}}>
            <Text style={{color:'#000', fontSize:18, height:height*0.06,}}>
          Help us get the word out !
          </Text>
          </View>
          <TextInput
              style={{height: height*0.06, width:'100%', backgroundColor:'#fff', textAlign: 'center'}}
              onChangeText={(text) => console.log({text})}
              placeholder="Referrer ID ( Optional )"
            />
            </View>
            </View>
            <View  style={{backgroundColor:'#ffffff33', paddingTop:height*0.025,paddingBottom:height*0.025, paddingLeft:width*0.05,paddingRight:width*0.05, marginTop:height*0.06, flexDirection:'column'}}>
            <View style={{height: height*0.06, width:'100%', flexDirection:'row', justifyContent:'flex-start', alignItems:'center'}}>
            <TextInput
              style={{height: height*0.055, width:'65%', backgroundColor:'#fff', textAlign: 'center'}}
              onChangeText={(text) => console.log({text})}
              placeholder="Gift Code"
            />
            <TouchableOpacity style={{ height: height*0.06,alignItems:'center', justifyContent:'center',
      backgroundColor: '#343a40',width:'35%',  }}>
          <Text style={{color:'#fff',fontSize:16,alignSelf:'center'}}>
          Redeem
          </Text>
          </TouchableOpacity>
          </View>
          <Text style={{color:'#000', fontSize:18, height:height*0.06,}}>
          Total $35
          </Text>
                    </View>
          </View>
          </ScrollView>
          </ImageBackground>
        {/*<KeyboardAwareScrollView style={styles.form} enableOnAndroid>
          {Config.shipping.visible && shippings.length > 0 && (
            <View>
              <View style={css.rowEmpty}>
                <Text style={[css.label, { color: text }]}>
                  {Languages.ShippingType}
                </Text>
              </View>

              <ScrollView contentContainerStyle={styles.shippingMethod}>
                {shippings.map((item, index) => (
                  <ShippingMethod
                    item={item}
                    key={`${index}shipping`}
                    onPress={this.selectShippingMethod}
                    selected={
                      (index == 0 && isShippingEmpty) ||
                      item.id == shippingMethod.id
                    }
                  />
                ))}
              </ScrollView>
            </View>
          )}

          <View style={css.rowEmpty}>
            <Text style={[css.label, { color: text }]}>
              {Languages.YourDeliveryInfo}
            </Text>
          </View>

          <View style={styles.formContainer}>
            <Form
              ref="form"
              type={this.Customer}
              options={this.options}
              value={this.state.value}
              onChange={this.onChange}
            />
          </View>
        </KeyboardAwareScrollView>*/}

        <Buttons
          isAbsolute
          onPrevious={this.props.onPrevious}
          onNext={this.nextStep}
        />
      </View>
    );
  }
}

Delivery.defaultProps = {
  shippings: [],
  shippingMethod: {},
  selectedAddress: {},
};

const mapStateToProps = ({ carts, user, countries, addresses }) => {
  return {
    user,
    customerInfo: carts.customerInfo,
    message: carts.message,
    type: carts.type,
    isFetching: carts.isFetching,
    shippings: carts.shippings,
    shippingMethod: carts.shippingMethod,
    countries: countries.list,
    selectedAddress: addresses.selectedAddress,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const CartRedux = require("@redux/CartRedux");
  const AddressRedux = require("@redux/AddressRedux");

  return {
    ...ownProps,
    ...stateProps,
    validateCustomerInfo: (customerInfo) => {
      CartRedux.actions.validateCustomerInfo(dispatch, customerInfo);
    },
    getShippingMethod: (zoneId) => {
      CartRedux.actions.getShippingMethod(dispatch, zoneId);
    },
    selectShippingMethod: (shippingMethod) => {
      CartRedux.actions.selectShippingMethod(dispatch, shippingMethod);
    },
    updateSelectedAddress: (address) => {
      AddressRedux.actions.updateSelectedAddress(dispatch, address);
    },
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(Delivery));

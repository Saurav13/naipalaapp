/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Languages } from "@app/Omni";
import EvilIcons from "@expo/vector-icons/EvilIcons";
import { Text, View, StyleSheet, ScrollView,TextInput, Dimensions, TouchableOpacity, Image,ImageBackground, Picker, Alert} from 'react-native';
import DateTimePicker from "react-native-modal-datetime-picker";
const {height, width} = Dimensions.get('window');

class ReviewPurchaseScreen extends React.Component {
  static propTypes = {
    onBack: PropTypes.func,
  };

  constructor(props) {
    super(props);

    // const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      // dataSource: ds.cloneWithRows(props.wishListItems),
      isDateTimePickerVisible: false
    };
  }
  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
  
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
  
  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
    this.hideDateTimePicker();
  };

  render() {
  const {
  updateSelectedIndex
} = this.props;
    return (
      		<ScrollView style={{flex: 1,}}>
      		       <View
      		        style={{height:'100%', paddingLeft:width*0.05, paddingRight:width*0.05, paddingBottom:10,backgroundColor:'transparent', marginTop:height*0.15,}}
      		      >
      		        <View style={{width: '100%', flexDirection:'column', backgroundColor:'transparent', marginBottom:10,}}>
      		          <Text style={{
      		          color:'#000',
      		          fontSize:20,
      		          fontWeight:'500',
      		          marginTop:10,
      		        }}>
      		          Review Purchase
      		        </Text>
      		        <View style={{width: '100%', borderBottomColor:'%000', borderBottomWidth:1, marginTop:5}}/>
                    <View style={{
                      flex: 1,
                      flexDirection: 'column',
                      justifyContent: 'space-between',
                      marginTip:10,
                    }}>
                              <View style={{width: '100%', height:height*0.05 ,  backgroundColor: '#ffffff08',}}>
                                <Text>
                                <Text style={{ fontSize:16, fontWeight:'700'}}>
                                  Amount:{' '}

                                </Text>
                                <Text style={{ fontSize:16,}}>
                                  $ 25.00
                                </Text>
                                </Text>
                              </View>
                      <View style={{width: '100%', height:height*0.05 ,  backgroundColor: '#ffffff08',}}>
                                <Text>
                                <Text style={{ fontSize:16, fontWeight:'700'}}>
                                  Sender Name:{' '}
                                </Text>
                                <Text style={{ fontSize:16,}}>
                                  Shrawan Bishowkarma {'\n'}
                                </Text>
                                </Text>
                              </View>
                       <View style={{width: '100%', height:height*0.05 ,  backgroundColor: '#ffffff08',}}>
                                <Text>
                                <Text style={{ fontSize:16, fontWeight:'700'}}>
                                  Recipient Name:{' '}
                                </Text>
                                <Text style={{ fontSize:16,}}>
                                  Shrawan Bishowkarma {'\n'}
                                </Text>
                                </Text>
                              </View>
                       <View style={{width: '100%', height:height*0.05 ,  backgroundColor: '#ffffff08',}}>
                                <Text>
                                <Text style={{ fontSize:16, fontWeight:'700'}}>
                                  Recipient Email:{' '}
                                </Text>
                                <Text style={{ fontSize:16,}}>
                                  bk@bksafsdcasdf.com {'\n'}
                                </Text>
                                </Text>
                              </View>
                       <View style={{width: '100%', height:height*0.05 ,  backgroundColor: '#ffffff08',}}>
                                <Text>
                                <Text style={{ fontSize:16, fontWeight:'700'}}>
                                  Delivery Date:{' '}
                                </Text>
                                <Text style={{ fontSize:16,}}>
                                  21 May, 2018 {'\n'}
                                </Text>
                                </Text>
                              </View>
                      <View style={{width: '100%',flexDirection:'column', backgroundColor: '#ffffff08',}}>
                                <Text style={{ fontSize:16, fontWeight:'700'}}>
                                  Message:{' '}
                                </Text>
                        <View style={{    borderRadius: 2,
                  borderWidth: 3,
                  borderColor: '#d6d7da', padding:10, marginTop:10,}}>
                          <Text style={{ fontSize:16,}}>
                          ustifyContent to a com ponen t's style deter mines the distrib ution of children along the primary axis. Should children be distributed at the start, the center, the end, or spaced evenly?
                          </Text>
                        </View>
                              </View>
                      <View style={{width: '100%', height: height*0.04, marginTop:20,flexDirection:'row',  justifyContent:'space-between'}}>
                        <TouchableOpacity style={{ alignItems:'center',
              backgroundColor: '#343a40',
              paddingTop: 4,width:'30%' }}
              onPress={()=>updateSelectedIndex('contactInformation')}>
                  <Text style={{color:'#fff',fontSize:16,}}>
                  Back
                  </Text>
                  </TouchableOpacity><TouchableOpacity style={{ alignItems:'center',
              backgroundColor: '#343a40',
              paddingTop: 4,width:'30%' }}
              onPress={()=>{
                        Alert.alert(
                          'Purchase Reviewed',
                          'Please make the payment',
                          [
                            {
                              text: 'Cancel',
                              onPress: () => console.log('Cancel Pressed'),
                              style: 'cancel',
                            },
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                          ],
                          {cancelable: false},
                        )
                      }}>
                  <Text style={{color:'#fff',fontSize:16,}}>
                  Proceed
                  </Text>
                  </TouchableOpacity>

                      </View>
                    </View>
      		          
      		        </View>
      		        </View>
      		              </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    wishListItems: state.wishList.wishListItems,
    cartItems: state.carts.cartItems,
  };
};
export default connect(mapStateToProps)(ReviewPurchaseScreen);

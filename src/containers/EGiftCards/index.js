/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Languages } from "@app/Omni";
import EvilIcons from "@expo/vector-icons/EvilIcons";
import styles from "./styles";
import { Text, View, StyleSheet, ScrollView,TextInput, Dimensions, TouchableOpacity, Image,ImageBackground, Picker} from 'react-native';
import ContactInformationScreen from './ContactInformation';
import ReviewPurchaseScreen from './ReviewPurchase';
const {height, width} = Dimensions.get('window');

class EGiftCardsScreen extends React.Component {
  static propTypes = {
    onBack: PropTypes.func,
  };

  constructor(props) {
    super(props);

    // const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      selectedIndex: 'eGiftCard',
    };

  }
  updateSelectedIndex = (selectedIndex) => {
    this.setState({ selectedIndex });
  };
  render() {
    const  {
      selectedIndex,
    } = this.state;
    return (
       <ImageBackground source={require("@images/body2.jpg")} style={{height:'100%', width:'100%',}}>
      { selectedIndex ==='eGiftCard' &&
        <ScrollView style={{flex: 1,}}>
       <View
        style={{height:'100%', paddingLeft:width*0.05, paddingRight:width*0.05, paddingBottom:10,backgroundColor:'transparent', marginTop:height*0.15,}}
      >
        <View style={{width: '100%', height: height*0.4, flexDirection:'column',}}>
          <Image source={{uri:'https://naipala.com/frontend-assets/images/gc3.png'}} style={{height:'72%', width:'100%'}}/>
          <View style={{width: '100%', height:'36%', flexDirection:'row', alignItems:'flex-start', justifyContent:'space-evenly', marginTop:15}}>
          <Image source={{uri:'https://naipala.com/frontend-assets/images/gc.png'}} style={{width:'23%', height:'46%'}}/>
           <Image source={{uri:'https://naipala.com/frontend-assets/images/gc1.png'}} style={{width:'23%', height:'46%'}}/>
            <Image source={{uri:'https://naipala.com/frontend-assets/images/gc2.png'}} style={{width:'23%', height:'46%'}}/>
             <Image source={{uri:'https://naipala.com/frontend-assets/images/gc3.png'}} style={{width:'23%', height:'46%'}}/>
          </View>
        </View>
        <View style={{width: '100%', height: height*0.8, flexDirection:'column', backgroundColor:'transparent', marginBottom:10,}}>
          <Text style={{
          color:'#000',
          fontSize:18,
          fontWeight:'600',
          marginTop:10,
        }}>
          NAIPALA E-GIFT CARD
        </Text>
        <View style={{width: '100%', borderBottomColor:'%000', borderBottomWidth:1, marginTop:5}}/>
          <View style={{width: '100%', height:'12%', flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:15}}>
          <Text style={{
            color:'#444',
            fontSize:18,
            fontWeight:'600',
          }}>
            Amount
          </Text>
          <View style={{width: width*0.65, height:height*0.05, justifyContent:'center', backgroundColor:'#fff'}}
         >
          <Picker
            onValueChange={(itemValue, itemIndex) =>
              console.log("itemValue-",itemValue)
            }
            mode='dropdown'
            style={{height:'100%', width: '100%'}}
            placeholder="Pick your amount"
          >
            <Picker.Item label="Choose amount" value=""/>
            <Picker.Item label="$25" value="$25"/>
            <Picker.Item label="$25" value="$25" />
            <Picker.Item label="$25" value="$25" />
            <Picker.Item label="$25" value="$25" />
            <Picker.Item label="$25" value="$25" />
            <Picker.Item label="$25" value="$25" />
          </Picker>
          </View>
        </View>
          <View style={{width: '100%', height:'12%', flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:15}}>
          <Text style={{
            color:'#444',
            fontSize:18,
            fontWeight:'600',
          }}>
            Recipent Name
          </Text>
          <TextInput
            style={{height: 30, width:'75%', backgroundColor:'#fff'}}
            onChangeText={(text) => console.log({text})}
          />
          </View>
          <View style={{width: '100%', height:'12%', flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:15}}>
          <Text style={{
            color:'#444',
            fontSize:18,
            fontWeight:'600',
          }}>
            Sender Name
          </Text>
          <TextInput
            style={{height: 30, width:'75%', backgroundColor:'#fff'}}
            onChangeText={(text) => console.log({text})}
          />
          </View>
          <View style={{width: '100%', height:'24%', flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:15}}>
          <Text style={{
            color:'#444',
            fontSize:18,
            fontWeight:'600',
          }}>
            Message
          </Text>
          <TextInput
            style={{width:'75%', backgroundColor:'#fff'}}
            onChangeText={(text) => console.log({text})}
            multiline = {true}
            numberOfLines = {5}
          />
          </View>
          <TouchableOpacity style={{ alignItems:'center',
    backgroundColor: '#343a40',
    paddingTop: 4,paddingBottom: 4,width:'40%' }}
    onPress={()=>this.updateSelectedIndex('contactInformation')}>
        <Text style={{color:'#fff',fontSize:16,}}>
        Proceed
        </Text>
        </TouchableOpacity>
        </View>
        </View>
              </ScrollView>}
        {
          selectedIndex==='contactInformation' && <ContactInformationScreen updateSelectedIndex={this.updateSelectedIndex}/>
        }
        {
          selectedIndex==='reviewPurchase' && <ReviewPurchaseScreen updateSelectedIndex={this.updateSelectedIndex}/>
        }
        </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    wishListItems: state.wishList.wishListItems,
    cartItems: state.carts.cartItems,
  };
};
export default connect(mapStateToProps)(EGiftCardsScreen);

/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Languages } from "@app/Omni";
import EvilIcons from "@expo/vector-icons/EvilIcons";
import styles from "./styles";
import { Text, View, StyleSheet, ScrollView,TextInput, Dimensions, TouchableOpacity, Image,ImageBackground, Picker} from 'react-native';
import DateTimePicker from "react-native-modal-datetime-picker";
const {height, width} = Dimensions.get('window');

class ContactInformationScreen extends React.Component {
  static propTypes = {
    onBack: PropTypes.func,
  };

  constructor(props) {
    super(props);

    // const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      // dataSource: ds.cloneWithRows(props.wishListItems),
      isDateTimePickerVisible: false
    };
  }
  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
  
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
  
  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
    this.hideDateTimePicker();
  };

  render() {
  const {
  updateSelectedIndex
} = this.props;
    return (
      		<ScrollView style={{flex: 1,}}>
      		       <View
      		        style={{height:'100%', paddingLeft:width*0.05, paddingRight:width*0.05, paddingBottom:10,backgroundColor:'transparent', marginTop:height*0.15,}}
      		      >
      		        <View style={{width: '100%', flexDirection:'column', backgroundColor:'transparent', marginBottom:10,}}>
      		          <Text style={{
      		          color:'#000',
      		          fontSize:20,
      		          fontWeight:'500',
      		          marginTop:10,
      		        }}>
      		          Contact Information
      		        </Text>
      		        <View style={{width: '100%', borderBottomColor:'%000', borderBottomWidth:1, marginTop:5}}/>
      		          <View style={{width: '100%', height:height*0.1, flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:15}}>
      		          <Text style={{
      		            color:'#444',
      		            fontSize:18,
      		            fontWeight:'400',
      		          }}>
      		            Sender Email
      		          </Text>
      		          <TextInput
      		            style={{height: 30, width:'100%', backgroundColor:'#fff'}}
      		            onChangeText={(text) => console.log({text})}
      		          />
      		          </View>
      		          <View style={{width: '100%', height:height*0.1, flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:15}}>
      		          <Text style={{
      		            color:'#444',
      		            fontSize:18,
      		            fontWeight:'400',
      		          }}>
      		            Recipient Email
      		          </Text>
      		          <TextInput
      		            style={{height: 30, width:'100%', backgroundColor:'#fff'}}
      		            onChangeText={(text) => console.log({text})}
      		          />
      		          </View>
      		          <View style={{width: '100%', height:height*0.1, flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:15}}>
      		          <Text style={{
      		            color:'#444',
      		            fontSize:18,
      		            fontWeight:'400',
      		          }}>
      		            Confirm Recipient Email
      		          </Text>
      		          <TextInput
      		            style={{width:'100%', backgroundColor:'#fff'}}
      		            onChangeText={(text) => console.log({text})}
      		          />
      		          </View>
      		          <View style={{width: '100%', height:height*0.1, flexDirection:'column', alignItems:'flex-start', justifyContent:'flex-start', marginTop:15}}>
      		          <Text style={{
      		            color:'#444',
      		            fontSize:18,
      		            fontWeight:'400',
      		            
      		          }}
      		            onPress={this.showDateTimePicker}

      		          >
      		            Delivery Date
      		          </Text>
      		           <TouchableOpacity style={{ alignItems:'center',
      		          backgroundColor: '#fff',
      		          height:height*0.05,
      		          paddingTop: 4,paddingBottom: 4,width:'100%' }}
      		          onPress={()=>this.showDateTimePicker()}/>
      		          {/*<View style={{width:'100%', height:height*0.05, backgroundColor:'#fff', }} onPress={this.showDateTimePicker}/>*/}
      		          <DateTimePicker
      			        isVisible={this.state.isDateTimePickerVisible}
      			        onConfirm={this.handleDatePicked}
      			        onCancel={this.hideDateTimePicker}
      			      />
      		          </View>
      		          <TouchableOpacity style={{ alignItems:'center',
      		    backgroundColor: '#343a40',
      		    paddingTop: 4,paddingBottom: 4,width:'40%' }}
      		    onPress={()=>updateSelectedIndex('reviewPurchase')}>
      		        <Text style={{color:'#fff',fontSize:16,}}>
      		        Next
      		        </Text>
      		        </TouchableOpacity>
      		        </View>
      		        </View>
      		              </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    wishListItems: state.wishList.wishListItems,
    cartItems: state.carts.cartItems,
  };
};
export default connect(mapStateToProps)(ContactInformationScreen);

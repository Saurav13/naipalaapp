/**
 * Created by InspireUI on 01/03/2017.
 *
 * @format
 */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Switch,
  LayoutAnimation,
  I18nManager,
  ImageBackground, Image,TouchableOpacity
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { WooWorker } from "api-ecommerce";
import { connect } from "react-redux";

import { Styles,Icons, Languages, Color, withTheme ,Config} from "@common";
import { toast, error, Validate, Icon, warn, FacebookAPI } from "@app/Omni";
import Button from "@components/Button";
import Spinner from "@components/Spinner";
import WPUserAPI from "@services/WPUserAPI";
import styles from "./styles";
import {ButtonIndex } from "@components";



class SignUpScreen extends Component {
  constructor(props) {
    super(props);

    let state = {
      name: "",
      email: "",
      password: "",
      useGeneratePass: false,
      isLoading: false,
    };

    const params = props.params;
    if (params && params.user) {
      state = { ...state, ...params.user, useGeneratePass: true };
    }

    this.state = state;

    this.onUsernameEditHandle = (name) => this.setState({ name });
    this.onEmailEditHandle = (email) => this.setState({ email });
    this.onPasswordEditHandle = (password) => this.setState({ password });

    this.onPasswordSwitchHandle = () =>
      this.setState({ useGeneratePass: !this.state.useGeneratePass });

    this.focusEmail = () => this.email && this.email.focus();
    this.focusPassword = () =>
      !this.state.useGeneratePass && this.password && this.password.focus();
  }

  shouldComponentUpdate() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    return true;
  }
  onFBLoginPressHandle = () => {
    const { login } = this.props;
    this.setState({ isLoading: true });
    FacebookAPI.login()
      .then(async (token) => {
        if (token) {
          const json = await WPUserAPI.loginFacebook(token);
          warn(["json", json]);
          if (json === undefined) {
            this.stopAndToast(Languages.GetDataError);
          } else if (json.error) {
            this.stopAndToast(json.error);
          } else {
            let customers = await WooWorker.getCustomerById(json.wp_user_id);
            customers = { ...customers, token, picture: json.user.picture };
            this._onBack();
            login(customers, json.cookie);
          }
        } else {
          this.setState({ isLoading: false });
        }
      })
      .catch((err) => {
        warn(err);
        this.setState({ isLoading: false });
      });
  };
  onSignUpHandle = async () => {
    const { login, netInfo } = this.props;
    if (!netInfo.isConnected) return toast(Languages.noConnection);

    const {
      name,
      email,
      password,
      useGeneratePass,
      isLoading,
    } = this.state;
    if (isLoading) return;
    this.setState({ isLoading: true });

    const _error = this.validateForm();
    if (_error) return this.stopAndToast(_error);

    const user = {
      name,
      email,
      password: useGeneratePass ? undefined : password,
    };
    const json = await WPUserAPI.register(user);
    if (json === undefined) {
      return this.stopAndToast("Server don't response correctly");
    } else if (json.error) {
      return this.stopAndToast(json.error);
    }
    const customer = await WooWorker.getCustomerById(json.user_id);
    if (customer) {
      this.setState({ isLoading: false });
      login(customer, json.cookie);
    } else {
      toast("Can't register user, please try again.");
    }
  };

  validateForm = () => {
    const {
      name,
      email,
      password,
      useGeneratePass,
    } = this.state;
    if (
      Validate.isEmpty(
        name,
        email,
        useGeneratePass ? "1" : password
      )
    ) {
      // check empty
      return "Please complete the form";
    } else if (!Validate.isEmail(email)) {
      return "Email is not correct";
    }
    return undefined;
  };

  stopAndToast = (msg) => {
    toast(msg);
    error(msg);
    this.setState({ isLoading: false });
  };

  render() {
    const {
      name,
      email,
      password,
      useGeneratePass,
      isLoading,
    } = this.state;
    const {
      theme: {
        colors: { background, text, placeholder },
      },
    } = this.props;

    const params = this.props.params;
    return (
      
      <View style={[styles.container, { backgroundColor: 'transparent' }]}>
      <ImageBackground source={require('./../../images/background.png')} style={{width: '100%', height: '100%'}}>

        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          enableOnAndroid>
          <View style={styles.formContainer}>
           
          <Image
            source={Config.LogoWithText}
            style={styles.logo}
            resizeMode="contain"
          />
           
           <View style={styles.loginTitle}>
              <Text style={{fontSize:20,fontWeight:'600'}}>
                {Languages.accountDetails}
              </Text>
            </View>
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.name = comp)}
              placeholder={Languages.username}
              onChangeText={this.onUsernameEditHandle}
              onSubmitEditing={this.focusEmail}
              autoCapitalize="none"
              returnKeyType="next"
              value={name}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.email = comp)}
              placeholder={Languages.email}
              onChangeText={this.onEmailEditHandle}
              onSubmitEditing={this.focusPassword}
              keyboardType="email-address"
              returnKeyType={useGeneratePass ? "done" : "next"}
              value={email}
              placeholderTextColor={placeholder}
            />
            {params && params.user ? (
              <View style={styles.switchWrap}>
                <Switch
                  value={useGeneratePass}
                  onValueChange={this.onPasswordSwitchHandle}
                  thumbTintColor={Color.accent}
                  onTintColor={Color.accentLight}
                />
                <Text
                  style={[
                    styles.text,
                    {
                      color: useGeneratePass
                        ? Color.accent
                        : Color.blackTextSecondary,
                    },
                  ]}>
                  {Languages.generatePass}
                </Text>
              </View>
            ) : null}
            {useGeneratePass ? (
              <View />
            ) : (
              <TextInput
                style={styles.input(text)}
                underlineColorAndroid="transparent"
                ref={(comp) => (this.password = comp)}
                placeholder={Languages.password}
                onChangeText={this.onPasswordEditHandle}
                secureTextEntry
                returnKeyType="done"
                value={password}
                placeholderTextColor={placeholder}
              />
            )}
            <Button
              containerStyle={styles.signUpButton}
              text={Languages.signup}
              onPress={this.onSignUpHandle}
            />
            <View style={styles.separatorWrap}>
            <View style={styles.separator(text)} />
            <Text style={styles.separatorText(text)}>{Languages.Or}</Text>
            <View style={styles.separator(text)} />
          </View>

          <ButtonIndex
            text={Languages.FacebookLogin.toUpperCase()}
            icon={Icons.MaterialCommunityIcons.Facebook}
            containerStyle={styles.fbButton}
            onPress={this.onFBLoginPressHandle}
          />
           
          </View>
          
          {isLoading ? <Spinner mode="overlay" /> : null}
        </KeyboardAwareScrollView>
        </ImageBackground>
      </View>
    );
  }
}



const mapStateToProps = (state) => {
  return {
    netInfo: state.netInfo,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { actions } = require("@redux/UserRedux");
  return {
    login: (user, token) => dispatch(actions.login(user, token)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTheme(SignUpScreen));

/** @format */

// @flow
/**
 * Created by InspireUI on 19/02/2017.
 */
import React, { PureComponent, Component } from "react";
import PropTypes from "prop-types";
import { View, ImageBackground , Text, Image, ScrollView, StyleSheet, Dimensions, WebView, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { Constants, withTheme, AppConfig, Color } from "@common";
import { HorizonList, ModalLayout, PostList } from "@components";
import { Font, LinearGradient } from 'expo';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import Ionicons from "@expo/vector-icons/Ionicons";

const { width } = Dimensions.get('window');
const height = width * 0.8;

const styless = StyleSheet.create({
  scrollContainer: {
    height,
    marginTop:width*0.2
  },
  image: {
    width,
    height,
  },
  imageAlone: {
    height,
    width: width*0.9,
  },
  container: {
    width:width,
    marginLeft:'5%',
    marginRight:'5%',
    alignItems:'center'
  },
});

// import Carousel from "react-native-snap-carousel";
import styles from "./styles";
class Carousel extends Component {
  render() {
    const { images, imageStyle, carouselStyle } = this.props;
    if (images && images.length) {
      return (
        <View
          style={carouselStyle}
        >
          <ScrollView
            horizontal
            pagingEnabled
            showsHorizontalScrollIndicator={true}
          >
          {console.log(images,'images')}
            {images.map(image => (
              <View>
              <Image key={image.image_link} style={imageStyle} source={{uri:'http://naipala.projectincube.com/uploads/'+image.image_link}} />
              </View>
            ))}
          </ScrollView>
        </View>
      );
    }
    console.log('Please provide images');
    return null;    
  }
}



class Home extends PureComponent {
  static propTypes = {
    fetchAllCountries: PropTypes.func.isRequired,
    layoutHome: PropTypes.any,
    onViewProductScreen: PropTypes.func,
    onShowAll: PropTypes.func,
    showCategoriesScreen: PropTypes.func,
  };
  constructor(props) {
    
    super(props);

    // const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      // dataSource: ds.cloneWithRows(props.wishListItems),
      landingImages:[],
      featuredItems:[],
      featuredImage:'',
      firstFeaturedImage:'',
      firstFeaturedTitle:'',
      firstFeaturedSubtitle:'',
      secondFeaturedImage:'',
      secondFeaturedTitle:'',
      secondFeaturedSubtitle:'',
      thirdFeaturedImage:'',
      thirdFeaturedTitle:'',
      thirdFeaturedSubtitle:'',
     




    };

  }
 

  fetchHome=async()=>{
    console.log('begin fetch');
     fetch('http://naipala.projectincube.com/api/home', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
     
    }).then((response) => response.json())
    .then((responseJson) => {
     console.log(responseJson)
      this.setState({
        landingImages:responseJson.landingImages,
        featuredItems:responseJson.featuredItems,
        featuredImage:responseJson.featuredImage,        
        firstFeaturedImage:responseJson.firstFeaturedImage,
        firstFeaturedTitle:responseJson.firstFeaturedTitle,
        firstFeaturedSubtitle:responseJson.firstFeaturedSubtitle,
        secondFeaturedImage:responseJson.secondFeaturedImage,
        secondFeaturedTitle:responseJson.secondFeaturedTitle,
        secondFeaturedSubtitle:responseJson.secondFeaturedSubtitle,
        thirdFeaturedImage:responseJson.thirdFeaturedImage,
        thirdFeaturedTitle:responseJson.thirdFeaturedTitle,
        thirdFeaturedSubtitle:responseJson.thirdFeaturedSubtitle,
        trendingTitle:responseJson.trendingTitle,
        trendingDescription:responseJson.trendingDescription,
        storyTitle:responseJson.storyTitle,
        storySubtitle:responseJson.storySubtitle,
        storyDescription:responseJson.storyDescription,
        
      });
    })
    .catch((error) => {
      console.error(error);
    });

  
  
  }
  async componentDidMount() {
    const { fetchAllCountries, fetchCategories, isConnected } = this.props;
    if (isConnected) {
      fetchAllCountries();
      fetchCategories();
    }
    console.log('fetchome');
    this.fetchHome();
    console.log('endfetchome');

  }
 

  render() {
    const {
      layoutHome,
      onViewProductScreen,
      showCategoriesScreen,
      onShowAll,
      theme: {
        colors: { background },
      },
    } = this.props;

    const isHorizontal = layoutHome == Constants.Layout.horizon || layoutHome == 7;
    const images = [
      {
        source: {
          uri: 'https://naipala.com/asset/uploads/1553057503664.jpg/1360/800',
        },
      },
      {
        source: {
          uri: 'https://naipala.com/asset/uploads/1555176840193.jpg/1360/800',
        },
      },
      {
        source: {
          uri: 'https://naipala.com/asset/uploads/1555200227745.png/1360/800',
        },
      },   
    ];
    const imagesForApparals = [
      {
        source: {
          uri: 'https://naipala.com/asset/uploads/1545031688304I0.png/290/290',
          },
        },
        {
          source: {
            uri: 'https://naipala.com/asset/uploads/1545128084527I0.png/290/290',
          },
        },
        {
          source: {
            uri: 'https://naipala.com/asset/uploads/1548503693915I0.png/290/290',
          },
        },
    ]
    return (
      <View style={styles.container}>
          <ImageBackground source={require("@images/body2.jpg")} style={{width: '100%', height: '100%'}}>
        
        <ScrollView>
            <Carousel images={this.state.landingImages} imageStyle={styless.image} carouselStyle={styless.scrollContainer}/>
              <View style={{flex:1, flexDirection: 'column', justifyContent:'space-around',alignItems:'center'}}>
                <Text style={styles.featuredCollection}>
                  FEATURED COLLECTION
                </Text>
                <Image
                  source={{uri: 'https://naipala.projectincube.com/frontend-assets/images/config_images/'+this.state.featuredImage}}
                  style={styless.imageAlone}
                />
                <View style={{marginTop:height*0.2, backgroundColor:"#cccccccc"}}>
                  <Image style={{width: width*0.9, height:width*0.9, backgroundColor:'transparent'}} source={{uri: 'https://naipala.projectincube.com/frontend-assets/images/config_images/'+this.state.firstFeaturedImage}}/>
                  <Text 
                    style={styles.text}
                  >
                    {this.state.firstFeaturedTitle}
                  </Text>
                  <Text 
                    style={{
                      textAlign:'center',
                      marginBottom: 10,
                      fontSize: 14,
                    }}
                  >
                    {this.state.firstFeaturedSubtitle}
                  </Text>
                  {/* <Text 
                    style={{
                      textAlign:'center',
                      fontWeight:'bold',
                      marginBottom: 10,
                      fontSize: 14,
                      fontFamily:'BarlowSemiCondensedBlack',
                    }}
                  >
                    Shop Now >
                  </Text> */}
                </View>
                <View style={{marginTop:height*0.2, backgroundColor:"#cccccccc"}}>
                  <Text 
                    style={{
                      textAlign:'center',
                      fontWeight:'bold',
                      marginTop: 20,
                      marginBottom: 10,
                      fontSize: 18,
                      fontFamily:'BarlowSemiCondensedBlack',
                      textTransform:'uppercase'
                    }}
                  >
                    {this.state.secondFeaturedTitle}
                  </Text>
                  <Text 
                    style={{
                      textAlign:'center',
                      marginBottom: 10,
                      fontSize: 14,
                    }}
                  >
                    {this.state.secondFeaturedSubtitle}
                    
                  </Text>
                  {/* <Text 
                    style={{
                      textAlign:'center',
                      fontWeight:'bold',
                      fontSize: 14,
                      fontFamily:'BarlowSemiCondensedBlack',
                    }}
                  >
                    Shop Now >
                  </Text> */}
                  <Image style={{width: width*0.9, height:width*0.9, backgroundColor:'transparent'}} source={{uri: 'https://naipala.projectincube.com/frontend-assets/images/config_images/'+this.state.secondFeaturedImage}}/>
                </View>
                <View style={{marginTop:height*0.2, backgroundColor:"#cccccccc"}}>
                  <Image style={{width: width*0.9, height:width*0.9, backgroundColor:'transparent'}} source={{uri: 'https://naipala.projectincube.com/frontend-assets/images/config_images/'+this.state.thirdFeaturedImage}}/>
                  <Text 
                    style={styles.text}
                  >
                    {this.state.thirdFeaturedTitle}
                    
                  </Text>
                  <Text 
                    style={{
                      textAlign:'center',
                      marginBottom: 10,
                      fontSize: 14,
                    }}
                  >
                    {this.state.thirdFeaturedSubtitle}
                    
                  </Text>
                  {/* <Text 
                    style={{
                      textAlign:'center',
                      fontWeight:'bold',
                      marginBottom: 10,
                      fontSize: 14,
                      fontFamily:'BarlowSemiCondensedBlack',
                    }}
                  >
                    Shop Now >
                  </Text> */}
                </View>
              </View>
            {/*<ParallaxScroll
              renderHeader={({ animatedValue }) => 
                <Image style={{width, height:width*0.20, backgroundColor: 'powderblue'}} source={require('./../../images/background.png')}/>
              }

              renderParallaxBackground={({ animatedValue }) => 
                <Image style={{width, height:height*2}} source={{uri: 'https://naipala.com/frontend-assets/images/config_images/1519138230387.jpg'}}/>
              }

              renderParallaxForeground={({ animatedValue }) => 
                <WebView
                  style={{marginTop:'40%',marginLeft:'5%', marginRight:'5%', flex:1, marginBottom:'20%'}}
                  javaScriptEnabled={true}
                  source={{
                    uri: 'https://youtu.be/diJk91lHIiE',
                  }}
                  scalesPageToFit
                />
              }

              headerHeight={height*0.50}
              isHeaderFixed = { false }
              parallaxHeight={height*2}
              parallaxBackgroundScrollSpeed={5}
              parallaxForegroundScrollSpeed={2.5}
            > 
            </ParallaxScroll>*/}
            <View style={{flex: 1, flexDirection: 'column', justifyContent:'flex-start',alignItems:'center'}}>
              <Text style={{
                textAlign:'center',
                fontWeight:'400',
                marginTop: width*0.2,
                fontSize: 18,
                fontFamily:'BarlowSemiCondensedSemiBold',
                textTransform:'uppercase'
              }}>
               TRENDING NOW
              </Text>
              <Text style={{
                textAlign:'center',
                fontWeight:'400',
                fontSize: 28,
                fontFamily:'BarlowSemiCondensedSemiBold',
                textTransform:'uppercase',
                marginLeft:'20%',
                marginRight:'20%',
              }}>
                {this.state.trendingTitle}
              </Text>
              <Text style={{
                textAlign:'center',
                fontSize: 16,
                marginTop:10
              }}>
                {this.state.trendingDescription}
              </Text>
              {/*<Image style={{width: width*0.8, height:width*0.8,}} source={{uri: 'https://naipala.com/asset/uploads/1545031688304I0.png/290/290'}}/>
              <Image style={{width: width*0.8, height:width*0.8,}} source={{uri: 'https://naipala.com/asset/uploads/1545128084527I0.png/290/290'}}/>
              <Image style={{width: width*0.8, height:width*0.8,}} source={{uri: 'https://naipala.com/asset/uploads/1548503693915I0.png/290/290'}}/>*/}
              <Carousel images={this.state.featuredItems} imageStyle={{height:height*0.9,width:width*0.8}} carouselStyle={{marginTop:0, height:height}}/>
            </View>
            <ParallaxScroll
              // renderHeader={({ animatedValue }) => 
              //   <Image style={{width, height:width*0.20, backgroundColor: 'powderblue'}} source={require('./../../images/background.png')}/>
              // }

              renderParallaxBackground={({ animatedValue }) => 
              <LinearGradient
                colors={['#ffffff99', '#ffffff99']}
                style={{ alignItems: 'center'}}
              >
                <Image style={{width, height:height*2, opacity:0.6}} source={{uri: 'https://naipala.com/frontend-assets/images/config_images/1535627912831.jpg'}}/>
              </LinearGradient>
              }

              renderParallaxForeground={({ animatedValue }) => 
                <View
                  style={{
                    height:'100%',
                    flexDirection:'column',
                    justifyContent: 'space-around',
                    alignItems:'center',
                    paddingLeft:'5%',
                    paddingRight:'5%'
                  }}
                >
                  <Text style={{
                    textAlign:'center',
                    fontSize: 34,
                    fontFamily:'BarlowSemiCondensedSemiBold',
                    textTransform:'uppercase',
                    marginTop:height*0.05,
                  }}>
                    {this.state.storyTitle}
                  </Text>

                  <Text style={{
                    textAlign:'center',
                    fontSize: 26,
                    fontFamily:'BarlowSemiCondensedSemiBold',
                    textTransform:'uppercase',
                  }}>
                    {this.state.storySubtitle}
                  </Text>
                  <Text style={{
                    textAlign:'center',
                    fontSize: 20,
                    fontFamily:'BarlowSemiCondensedSemiBold',
                  }}>
                    {this.state.storyDescription}
                  </Text>
                  <TouchableOpacity style={styles.button}>
                    <Text> LEARN MORE ABOUT NAIPALA </Text>
                  </TouchableOpacity>
                </View>
              }

              headerHeight={height*0.50}
              isHeaderFixed = { false }
              parallaxHeight={height*2}
              parallaxBackgroundScrollSpeed={5}
              parallaxForegroundScrollSpeed={2.5}
            >
            </ParallaxScroll>
            <View style={{flexDirection:'column', justifyContent:'space-around', alignSelf:'center', marginBottom:height*0.05,alignItems:'center', height:height*0.4}}>
            <Image source={{uri:'https://naipala.com/frontend-assets/images/naipala_logo.png'}} style={{height:height*0.15, width:height*0.45,}} />
            <View style={{width:width*0.5, flexDirection: 'row',  justifyContent:'space-around',  height:height*0.15,}}>
              <Ionicons
                name="logo-facebook"
                size={40}
                color='#000'
              />
              <Ionicons
                name="logo-instagram"
                size={40}
                color='#000'
              />
              <Ionicons
                name="md-mail"
                size={40}
                color='#000'
              />
            </View>
            </View>
        </ScrollView>
          </ImageBackground>

      </View>
    );
  }
}

const mapStateToProps = ({ user, products, countries, netInfo }) => ({
  user,
  layoutHome: products.layoutHome,
  countries,
  isConnected: netInfo.isConnected,
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const CountryRedux = require("@redux/CountryRedux");
  const { actions } = require("@redux/CategoryRedux");

  return {
    ...ownProps,
    ...stateProps,
    fetchCategories: () => actions.fetchCategories(dispatch),
    fetchAllCountries: () => CountryRedux.actions.fetchAllCountries(dispatch),
  };
}

export default withTheme(
  connect(
    mapStateToProps,
    undefined,
    mergeProps
  )(Home)
);

/** @format */

import { Platform, StyleSheet, Dimensions } from "react-native";
import { Constants } from "@common";

const { width } = Dimensions.get("window");
const vw = width / 100;

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginBottom:0,
    
  },
  toolbarIcon: {
    width: 17,
    height: 17,
    resizeMode: "contain",
    marginTop: 12,
    marginRight: 12,
    marginBottom: 12,
    marginLeft: 12,
    opacity: 0.8,
  },
  fill: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  header: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: "rgba(255, 255, 255, 0.5)",
    overflow: "hidden",
    height: Constants.Window.headerHeight,
  },
  logo: {
    width: vw * 30,
    resizeMode: "contain",
  },
  backgroundImage: {
    position: "absolute",
    top: 10,
    left: 0,
    right: 0,
    width: null,
    height:
      Platform.OS === "ios"
        ? Constants.Window.headerHeight
        : Constants.Window.headerHeight + 100,
  },
  toolbar: {
    backgroundColor: "transparent",
    marginTop: Platform.OS === "ios" ? 12 : 28,
    height: 32,
    alignItems: "center",
    justifyContent: "space-between",
    position: "absolute",
    top: 0,
    left: 7,
    right: 7,
    flexDirection: "row",
  },
  scrollViewContent: {
    marginTop: Constants.Window.headerHeight,
    position: "relative",
    marginBottom: 100,
  },
  columnStyle: {
    justifyContent: "space-between",
    marginTop: 10,
    marginRight: 10,
    marginBottom: 10,
    marginLeft: 10,
  },
  more: {
    width,
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10,
  },
  text: {
    textAlign:'center',
    fontWeight:'bold',
    marginTop: 10,
    marginBottom: 10,
    fontSize: 18,
    fontFamily:'BarlowSemiCondensedBlack',
    textTransform:'uppercase'
  },
  featuredCollection: {
    textAlign:'center',
    fontWeight:'bold',
    marginTop: width*0.1,
    marginBottom: width*0.1,
    fontSize: 24,
    marginLeft:'10%',
    marginRight:'10%',
    fontFamily: Constants.fontFamily,
  },
  button: {
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderRadius: 2,
    borderWidth: 2,
    borderColor: '#23272b',
    fontFamily: Constants.fontFamily,
    justifyContent: 'center',
    marginBottom: 10,
  },
});

import * as React from 'react';
import { Text, View, StyleSheet, Image, WebView, TouchableOpacity, ScrollView, TextInput,Dimensions, ImageBackground } from 'react-native';
import { Icon } from 'react-native-elements'
// import { WebView } from 'react-native-webview';
const {height, width} = Dimensions.get('window');

const styless = StyleSheet.create({
  scrollContainer: {
    height,
    marginTop:width*0.2
  },
  image: {
    width,
    height,
  },
  imageAlone: {
    height,
    width: width*0.9,
  },
  container: {
    width:width,
    marginLeft:'5%',
    marginRight:'5%',
    alignItems:'center'
  },
});

class Carousel extends React.Component {
  render() {
    const { images, imageStyle, carouselStyle } = this.props;
    if (images && images.length) {
      return (
        <View
          style={carouselStyle}
        >
          <ScrollView
            horizontal
            pagingEnabled
            showsHorizontalScrollIndicator={true}
          >
          {console.log(images,'images')}
            {images.map(image => (
              <View>
              <Image key={image.image_link} style={imageStyle} source={{uri:'http://naipala.projectincube.com/uploads/'+image.image_link}} />
              </View>
            ))}
          </ScrollView>
        </View>
      );
    }
    console.log('Please provide images');
    return null;    
  }
}

export default class SocialInfoScreen extends React.Component {
  render() {
    const images = [
      {
        source: {
          uri: 'https://naipala.com/asset/uploads/1553057503664.jpg/1360/800',
        },
      },
      {
        source: {
          uri: 'https://naipala.com/asset/uploads/1555176840193.jpg/1360/800',
        },
      },
      {
        source: {
          uri: 'https://naipala.com/asset/uploads/1555200227745.png/1360/800',
        },
      },   
    ];
    return (
<ImageBackground style={{height:'100%', width:'100%'}}
        source={require("@images/body2.jpg")}
      >
      <ScrollView style={{padding:10, marginBottom:20,}}>
        <View style={{width: '100%', backgroundColor:'transparent',height: height*0.1, justifyContent:'center', alignItems:'center',marginTop:height*0.1}} >
          <Text style={{fontSize:24, fontWeight:'500' }}>
            Social
          </Text>
        </View>
      <View style={{flex:1, padding:10, backgroundColor:'#ffffff8c', borderRadius:4,}}>
        <View style={{width: '100%', height: height*0.06, justifyContent:'flex-start', alignItems:'center',}} >
          <Text style={{fontSize:24, }}>
            Socialite
          </Text>
        </View>
        <View style={{width: '100%', height: height*0.4, justifyContent:'center', alignItems:'center',}}>
          <Image
            style={{height:height*0.3,width:height*0.25}}
            source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}

        />
            {/*<Carousel images={images} imageStyle={styless.image} carouselStyle={styless.scrollContainer}/>*/}

        </View>
        <View style={{width: '100%', height: height*0.1, justifyContent:'center', alignItems:'flex-start'}} >
          <Text style={{fontSize:16}}>
            NAIPALA STORIES | 
          </Text>
          <Text>
          <Text style={{fontSize:14}}>
            on April 3, 2019
          </Text>
          <Text style={{fontSize:14, fontStyle: 'italic'}}>
            / by Meghan Horvath
          </Text>
          </Text>
        </View>
        <View style={{width: '100%', height: height*0.3, justifyContent:'center', alignItems:'flex-start'}} >
          <Text style={{fontSize:18, fontWeight:'400'}}>
            Automatically update your Slack status based on your calendar, respond to meeting invites, send emails into Slack, and more with the new Office 365 and G Suite apps for Slack.Connect the tools you use every day like your calendar, email, and files with Slack to simplify common, everyday tasks.
          </Text>
        </View>
        <View style={{width: '100%', height: height*0.06, justifyContent:'space-around',alignItems:'center', flexDirection:'row'}} >
        <View style={{width:'25%', flexDirection:'row',  justifyContent:'space-around',alignItems:'center', }}>
          <Icon
            name='heart'
            type='font-awesome'
            color='#f50'
            onPress={() => console.log('hello')} />
          <Text style={{fontSize:18, fontWeight:'400'}}>Like</Text>
          </View>
          <View style={{width:'75%',  flexDirection:'row',  justifyContent:'space-around',alignItems:'center', }}>
          <Text style={{fontSize:18, fontWeight:'400'}}>10 likes</Text>
          <Text style={{fontSize:18, fontWeight:'400'}}>20 comments</Text>
          </View>          
        </View>
        <View style={{marginTop:height*0.02,}}>
          <Text style={{fontSize:20, fontWeight:'400'}}>
            Comments:
          </Text>
          {/*comment 1 */}
          <ImageBackground style={{width:'100%', marginBottom:height*0.02}}
                  source={require("@images/body2.jpg")}
                >
          <View style={{backgroundColor:'transparent', justifyContent:'space-around',padding:10,}}>
          <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start', }}>
          <View style={{width:'20%'}}>
            <Image
            style={{height:width*0.12,width:width*0.12}}
            source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
        /></View>
        <Text style={{fontSize:16, fontWeight:'400'}}>
          Saurav Shrestha
        </Text>
        </View>
        <Text style={{fontSize:14, fontWeight:'400'}}>
          component's style determines the distribution of children along the primary axis. Should children be distributed at the start, the center, the end, or spa
        </Text>
          </View>
          </ImageBackground>
        {/*comment 2*/}
        <ImageBackground style={{width:'100%', marginBottom:height*0.02}}
                source={require("@images/body2.jpg")}
              >
        <View style={{backgroundColor:'transparent', justifyContent:'space-around',padding:10,}}>
          <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-start', }}>
          <View style={{width:'20%'}}>
            <Image
            style={{height:width*0.12,width:width*0.12}}
            source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
        /></View>
        <Text style={{fontSize:16, fontWeight:'400'}}>
          Saurav Shrestha
        </Text>
        </View>
        <Text style={{fontSize:14, fontWeight:'400'}}>
          component's style determines the distribution of children along the primary axis. Should children be distributed at the start, the center, the end, or spa
        </Text>
          </View>
          </ImageBackground>
        </View>
        {/*comment 3*/}
        
        <View style={{height:height*0.25, alignItems:'flex-end', marginBottom:height*0.05}}>
        <TextInput
          style={{
            backgroundColor:'#E8E8E7',
            width:'100%',
            color:'black',
            padding:20,
            height:'80%',
            borderColor: 'gray', 
            borderWidth: 1,
          }}
          // onChangeText={(text) => this.setState({text})}
          placeholder="What's on your mind?"
          multiline
        />
          <TouchableOpacity style={[styles.addReviewButton,{marginTop:10}]}>
            <Text style={{color:'#000', fontSize:14,fontWeight:'500'}}>
              COMMENT
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      </ScrollView>
      </ImageBackground>
    
    );
  }
}

const styles = StyleSheet.create({
    addReviewButton: {
    backgroundColor: 'transparent',
    borderRadius: 2,
    borderWidth: 2,
    borderColor: '#23272b',
    justifyContent: 'center',
    height:width*0.1,
    width:width*0.3,
    alignItems:'center',
  },
});

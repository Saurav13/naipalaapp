/** @format */

import React from "react";
import { View, Text, TouchableOpacity, ScrollView, ImageBackground } from "react-native";
import { connect } from "react-redux";

import { Languages, withTheme, Tools, Color } from "@common";
import { ProductCatalog, ProductTags } from "@components";
import Slider from "react-native-fluid-slider";
import SocialScreen from './Social'
class InfoScreen extends React.PureComponent {
  render() {
    return (
      <SocialScreen/>
    );
  }
}

InfoScreen.defaultProps = {
  tags: [],
};

const mapStateToProps = (state) => {
  return {

  };
};

// function mergeProps(stateProps, dispatchProps, ownProps) {
// }

export default connect(
  mapStateToProps,
  null,
  // mergeProps
)(withTheme(InfoScreen));

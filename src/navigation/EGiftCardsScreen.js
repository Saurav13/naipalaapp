/** @format */

import React, { Component } from "react";
import { Text } from 'react-native'
import { Languages, Color, Styles, withTheme } from "@common";
import { EGiftCards } from "@containers";
import { Menu, NavBarLogo, HeaderHomeRight  } from "./IconNav";

@withTheme
export default class EGiftCardsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );
    const dark = navigation.getParam("dark", false);

    return {
      headerTitle: NavBarLogo({ navigation }),
      headerLeft: Menu(dark),
      // headerRight: HeaderHomeRight(navigation),

      headerTintColor: Color.headerTintColor,
      headerStyle,
      headerTitleStyle: Styles.Common.headerStyle,

      // use to fix the border bottom
      headerTransparent: true,
    };
  };
  componentWillMount() {
    const {
      theme: {
        colors: { background },
        dark,
      },
    } = this.props;

    this.props.navigation.setParams({
      headerStyle: Styles.Common.toolbar(background, dark),
      dark,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.theme.dark !== nextProps.theme.dark) {
      const {
        theme: {
          colors: { background },
          dark,
        },
      } = nextProps;
      this.props.navigation.setParams({
        headerStyle: Styles.Common.toolbar(background, dark),
        dark,
      });
    }
  }

  render() {
    return <EGiftCards/>;
  }
}

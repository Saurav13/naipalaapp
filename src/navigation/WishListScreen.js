/** @format */

/*import React, { PureComponent } from "react";
import { Menu, EmptyView } from "./IconNav";

import { Images, Config, Constants, Color, Styles, Languages } from "@common";
import { WishList } from "@containers";

export default class WishListScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: Languages.WishList,
    header: null,
  });

  render() {
    const { navigate } = this.props.navigation;
    // const rootNavigation = this.props.screenProps.rootNavigation;

    return (
      <WishList
        onViewProduct={(product) => navigate("Detail", product)}
        onViewHome={() => navigate("Default")}
      />
    );
  }
}


*/

/** @format */

import React, { PureComponent } from "react";

import { Color, Styles, withTheme } from "@common";
import { WishList } from "@containers";
import { Logo, Menu } from "./IconNav";

@withTheme
export default class WishListScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );
    const dark = navigation.getParam("dark", false);

    return {
      headerTitle: Logo(),
      headerLeft: Menu(dark),

      headerTintColor: Color.headerTintColor,
      headerStyle,
    };
  };

  componentWillMount() {
    const {
      theme: {
        colors: { background },
        dark,
      },
    } = this.props;

    this.props.navigation.setParams({
      headerStyle: Styles.Common.toolbar(background, dark),
      dark,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.theme.dark !== nextProps.theme.dark) {
      const {
        theme: {
          colors: { background },
          dark,
        },
      } = nextProps;
      this.props.navigation.setParams({
        headerStyle: Styles.Common.toolbar(background, dark),
        dark,
      });
    }
  }

  render() {
    const { navigation } = this.props;

    return <WishList navigation={navigation} />;
  }
}

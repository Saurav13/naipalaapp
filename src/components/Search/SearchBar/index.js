/** @format */

import React from "react";
import {
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Animated,
  ImageBackground,
} from "react-native";
import styles from "./style";
import Icon from "react-native-vector-icons/Ionicons";
import { Icons, Languages, Color, withTheme } from "@common";

@withTheme
class SearchBar extends React.Component {
  render() {
    let {
      autoFocus,
      value,
      onChangeText,
      onSubmitEditing,
      scrollY,
      onClear,
      onFilter,
      isShowFilter,
      haveFilter,
    } = this.props;

    const {
      theme: {
        colors: {text, lineColor },
      },
    } = this.props;

    const transformY = scrollY.interpolate({
      inputRange: [0, 50],
      outputRange: [50, 0],
      extrapolate: "clamp",
    });

    return (
    <ImageBackground source={require("@images/body2.jpg")} style={{width: '100%', height: '100%'}}>
      <Animated.View
        style={[
          styles.container,
          {
            // transform: [{ translateY: transformY }],
          },
          { backgroundColor: 'transparent' },
        ]}>
        <Icon name={Icons.Ionicons.Search} size={20} color={text} />
        <TextInput
          placeholder={Languages.SearchPlaceHolder}
          placeholderTextColor={text}
          style={[styles.input, {color: text} ]}
          underlineColorAndroid="transparent"
          autoFocus={autoFocus}
          value={value}
          onChangeText={onChangeText}
          onSubmitEditing={onSubmitEditing}
        />
        {value.length > 0 && (
          <TouchableOpacity onPress={onClear}>
            <Image
              source={require("@images/ic_clear_search.png")}
              style={[styles.icon, { tintColor: text }]}
            />
          </TouchableOpacity>
        )}


          <View style={[styles.separator, { tintColor: text }]} />

          <TouchableOpacity onPress={onFilter}>
            <Image
              source={require("@images/ic_filter_search.png")}
              style={[
                styles.icon,
                { tintColor: haveFilter ? Color.primary : text },
              ]}
            />
          </TouchableOpacity>

      </Animated.View>
      </ImageBackground>
    );
  }
}

export default SearchBar;

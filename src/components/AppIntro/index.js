/** @format */

import React, { PureComponent } from "react";
import { Ionicons } from "@expo/vector-icons";
import { StyleSheet, View, Text, Image, I18nManager } from "react-native";
import { LinearGradient } from "@expo";
import AppIntroSlider from "react-native-app-intro-slider";
import styles from "./styles";
import { Config } from "@common";
import { connect } from "react-redux";

class AppIntro extends PureComponent {
  _renderItem = (props) => (
    <View
      style={[
        styles.mainContent,
        {
          paddingTop: props.topSpacer,
          paddingBottom: props.bottomSpacer,
          width: props.width,
          height: props.height,
        },
      ]}>

      <Text style={styles.title}>{props.title}</Text>
      <Image source={props.image} style={ styles.image } />

    </View>
  );

  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name={
            I18nManager.isRTL ? "md-arrow-round-back" : "md-arrow-round-forward"
          }
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: "transparent" }}
        />
      </View>
    );
  };

  _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name="md-checkmark"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: "transparent" }}
        />
      </View>
    );
  };

  render() {
    return (
      <AppIntroSlider
        slides={Config.intro}
        renderItem={this._renderItem}
        renderDoneButton={this._renderDoneButton}
        renderNextButton={this._renderNextButton}
        onDone={this.props.finishIntro}
      />
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  const { actions } = require("@redux/UserRedux");
  return {
    finishIntro: () => dispatch(actions.finishIntro()),
  };
};
export default connect(
  null,
  mapDispatchToProps
)(AppIntro);
